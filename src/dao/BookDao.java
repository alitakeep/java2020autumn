package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;
import javax.swing.ImageIcon;


import model.Book;

import util.DBUtil;
/**
 * 该类是处理Book实体与数据库连接的类
 * @author 10432
 *
 */
public class BookDao {
	
	/**
	 * 该方法用于获取图书信息表格中的所有数据
	 * @return	返回一个包含两个向量的散列映射
	 */
	public Map<Integer, Vector> searchAllBook() {
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		Vector<String> column = new Vector<String>();
		Map<Integer, Vector> mapOfVector = new HashMap<Integer, Vector>();
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery("select BOOKNUMBER as \"图书编号\",BOOKNAME as \"书名\",BOOKTYPE as \"图书类别\",BOOKCASE as \"图书状况\",AUTHOR as \"作者\" from book_information ");
			ResultSetMetaData rsMetaData = rs.getMetaData();
			//把表格的属性名添加到列向量中
			for(int i=1;i<=rsMetaData.getColumnCount();i++) {
				column.add(rsMetaData.getColumnName(i));
			}
			//把表格数据添加到行向量中
			while(rs.next()) {
				//每读一行,初始化一次行向量。
				Vector<String> row = new Vector<String>();
				for(int i=1;i<=rsMetaData.getColumnCount();i++) {
				row.add(rs.getString(i));
				}
				data.add(row);
			}
			rs.close();
			sta.close();
			DBUtil.closeConnection(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mapOfVector.put(1, data);
		mapOfVector.put(2, column);
		return mapOfVector;
	}
	
	/*public boolean changeBookInformation(String bookNumber,String changedName) {
		Connection con = null;
		StringBuffer sqlBuffer = new StringBuffer("update book_information set and =? where booknumber=?");
		int numOfExequte = 0;
		String sql = sqlBuffer.toString().replace("and",changedName);
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1,changedName);
			pre.setString(2, bookNumber);
			numOfExequte = pre.executeUpdate();
			
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		if(numOfExequte==1) {
			return true;
		}else {
			return false;
		}
	}*/
	/**
	 * 该方法用于处理图书管理员对图书信息的修改
	 * @param 
	 * @return
	 * @throws SQLException
	 */
	public boolean changeBookInformation(Book book)  {
		Connection con = null;
		boolean canChange = true;
		String sql = "update book_information set BOOKNAME=?,BOOKTYPE=?,AUTHOR=?,INTRODUCTION=?,PICTURE=? where booknumber=?";
		int numberOfExeute = 0;
		if(canChange) {
			try {
				con = DBUtil.getConnection();
				PreparedStatement pre = con.prepareStatement(sql);
				pre.setString(1, book.getBookName());
				pre.setString(2, book.getBookType());
				pre.setString(3, book.getAuthor());
				pre.setClob(4, book.getIntroduction());
				pre.setBlob(5, book.getPhoto());
				pre.setString(6, book.getBookNumber());
				numberOfExeute = pre.executeUpdate();
			
			} catch (SQLException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}finally {
				DBUtil.closeConnection(con);
			}
			//如果执行后修改了一行数据，则表示修改成功
			if(numberOfExeute==1) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		
	}
	/**
	 * 该方法用于查询给定名称的图书信息
	 * @param bookName  用户输入的图书信息
	 * @return
	 * @throws SQLException
	 */
	public Vector<Vector<String>> searchBook(String bookName)  {
		Vector<Vector<String>> data = null;
		//如果输入框的内容不为空，则执行查询
		if(bookName.equals("")!=true) {
			Connection con = null;
			data = new Vector<Vector<String>>();
			
			//为了便于搜索，此处使用模糊查询
			String sql = "select * from book_Information where bookname like ?";
			ResultSet rs = null;
			try {
				con = DBUtil.getConnection();
				PreparedStatement pre = con.prepareStatement(sql);
				pre.setString(1, "%"+bookName+"%");
				rs = pre.executeQuery();
				ResultSetMetaData rsMetaData = rs.getMetaData();
				//把查询的结果集封住入向量中
				while(rs.next()) {
					Vector<String> bookInformation = new Vector<String>();
					for(int i=1;i<=rsMetaData.getColumnCount();i++) {
						bookInformation.add(rs.getString(i));
					}
					/*考虑到model.setDataVector中的第一个参数必须是Vector<Vector<?extend Vector>>,
					 * 所以即便查询出来只有一条信息，也要把bookInformation 嵌套到另一个向量中。	*/
					data.add(bookInformation);
				}
				rs.close();
			} catch (SQLException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}finally {
				
				DBUtil.closeConnection(con);
			}
			return data;
		}
		//如果输入框的内容为空，则直接返回空值的data
		else {								
			return data;
		}
		
	}
	/**
	 * 该方法用于判断书籍信息表中是否有一样的书籍编号
	 * @param bookNumber
	 * @return 
	 */
	public boolean isOrNotInDatabase(String bookNumber)  {
		Connection con = null;
		String sql = "select bookNumber from book_information where booknumber = ?";
		PreparedStatement pre = null;
		int numberOfexe = 0;
		try {
			con = DBUtil.getConnection();
			pre = con.prepareStatement(sql);
			pre.setString(1, bookNumber);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				numberOfexe++;
			}
			
			rs.close();
			DBUtil.closeConnection(con);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		if(numberOfexe==1) {
		return true;
		}
		else {
			return false;
		}
	}
/**
 * 该方法用于处理删除图书信息
 * @param book
 * @return
 */
	public boolean delectBookInformation(Book book) {
		Connection con = null;
		String sql = "delete from book_information where booknumber = ?";
		int executeOfNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, book.getBookNumber());
			executeOfNum = pre.executeUpdate();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeOfNum==1) {
			return true;
		}
		else {
		return false;
		}
	}
/**
 * 该函数用于处理添加图书信息
 * @param book
 * @return 
 */
	public boolean addBooks(Book book) {
		boolean result = isOrNotInDatabase(book.getBookNumber());
		//如果增加的图书的编号不与数据库中已有的图书编号冲突的话，则可以添加。
		if(!result) {
			Connection con = null;
			String sql = "INSERT INTO BOOK_INFORMATION (BOOKNUMBER, BOOKNAME, BOOKTYPE, BOOKCASE, AUTHOR,INTRODUCTION,PICTURE) values(?,?,?,?,?,?,?)";
			int executeNum = 0;
			try {
				con = DBUtil.getConnection();
				PreparedStatement pre = con.prepareStatement(sql);
				pre.setString(1, book.getBookNumber());
				pre.setString(2, book.getBookName());
				pre.setString(3, book.getBookType());
				//添加图书时默认图书在馆
				pre.setString(4,"在馆");
				pre.setString(5, book.getAuthor());
				pre.setClob(6, book.getIntroduction());
				pre.setBlob(7, book.getPhoto());
				executeNum = pre.executeUpdate();
			} catch (SQLException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}finally {
				DBUtil.closeConnection(con);
			}
			if(executeNum==1) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	/**
	 * 该方法以传入的sql语句对数据库进行查询
	 * @param sql 传入的sql语句
	 * @return 返回一个包含着查询结果的二维向量
	 */
	public Vector<Vector<String>> search(String sql) {
		Connection con = null;
		Vector<Vector<String>> data = new Vector();
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery(sql);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			while(rs.next()) {
				Vector<String> row = new Vector<String>();
				for(int column=1;column<=rsMetaData.getColumnCount();column++) {
					row.add(rs.getString(column));
				}
				data.add(row);
			}
			sta.close();
			rs.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return data;
	}
	/**
	 * 从数组库中获取图片
	 * @param idOfBook 对应的图书编号
	 * @return
	 */
	public ImageIcon getImageIcon(String idOfBook) {
		Connection con = null;
		String sql = "select picture from book_information where booknumber=?";
		SerialBlob blob = null;
		ImageIcon icon = null;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, idOfBook);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				blob = new SerialBlob(rs.getBlob("picture"));
			}
			icon = new ImageIcon(blob.getBytes(1L,(int)blob.length()));
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return icon;
	}
	 /**
	  * 从数据库中获取书籍简介
	  * @param idOfBook 相应的书籍编号
	  * @return
	  */
	public String getArticle(String idOfBook) {
		Connection con = null;
		String sql = "select introduction from book_information where booknumber=?";
		SerialClob clob = null;
		String text = null;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, idOfBook);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				clob = new SerialClob(rs.getClob("introduction"));
			}
			text = clob.getSubString(1,(int) clob.length());
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return text;
	}
	/**
	 * 该方法用于获取相应图书编号的所有信息
	 * @param id
	 * @return 返回一个图书实体类
	 */
	public Book getBook(String id) {
		Connection con = null;
		String sql = "select * from book_information where booknumber=?";
		Book book  = new Book();
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, id);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				book.setBookNumber(id);
				book.setBookName(rs.getString("bookname"));
				book.setAuthor(rs.getString("author"));
				book.setBookCase(rs.getString("bookcase"));
				book.setBookType(rs.getString("booktype"));
				book.setPhoto(new SerialBlob(rs.getBlob("picture")));
				book.setIntroduction(new SerialClob(rs.getClob("introduction")));
			}
			rs.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return book;
	}
	}
