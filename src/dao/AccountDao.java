package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import model.Account;
import model.User;
import util.DBUtil;

public class AccountDao {
	/**
	 * 该方法用于获取相应账号的个人信息
	 * @param user 封装了用户账号的实体类
	 * @return 返回一个封装了对应账号信息的实体类
	 */
	public Account getAccount(User user) {
		Connection con = null;
		String sql = "select * from account_information where id = ?";
		String id = null;
		String name = null;
		String gender = null;
		long phone = 0;
		String email = null;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, user.getId());
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				id = rs.getString(1);
				name = rs.getString(2);
				gender = rs.getString(3);
				email = rs.getString(4);
				phone = rs.getLong(5);
			}
			rs.close();
		} catch (Exception e) {
			user.setId("null");
		}finally {
			DBUtil.closeConnection(con);
		}
		return new Account(id, name, gender,email,phone);
	}
	/**
	 * 该方法用于修改个人信息
	 * @param account
	 * @return 返回一个布尔类型的值表示修改成功与否
	 */
	public boolean changedInformation(Account account) {
		Connection con = null;
		String sql = "update account_information set name=?,phone=?,email=? where id=?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, account.getName());
			pre.setLong(2,account.getPhone());
			pre.setString(3, account.geteMail());
			pre.setString(4, account.getId());
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if (executeNum==1) {
			return true;
		}else {
			return false;
		}
		
	}
	/**
	 * 该方法针对不同的用户创建不同的视图。视图的建立需要避免重名，为此用用户的账号加在视图之后以互相区别。
	 * @param idOfUser 该登录用户的账号
	 *
	 */
	public void createView(String idOfUser) {
		Connection con = null;
		String sql = "create view lending_"+idOfUser+"" 
				+"(\"图书编号\",\"书名\",\"作者\",\"归还日期\",\"续借次数\")" 
				+"as select APP.book_information.BOOKNUMBER,APP.book_information.BOOKNAME,APP.book_information.AUTHOR," 
				+" APP.lending_information.DATEOFRETURN,APP.lending_information.NUMBEROFRENEW" 
				+" from APP.book_information,APP.lending_information" 
				+" where lending_information.IDOFBOOK=book_information.BOOKNUMBER and lending_information.IDOFBORROWER='"+idOfUser+"'";
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			sta.execute(sql);
			sta.close();
		} catch (Exception e) {
		}finally {
			DBUtil.closeConnection(con);
		}
		
	}
	/**
	 * 该方法用于查询针对该用户生成的视图来获取该用户的借书信息。
	 * @param IdOfUser
	 * @return 返回一个包含了两个向量的散列映射。其中的两个向量分别包括表的字段名和与字段名对应的数据。
	 */
	public Map<String, Vector> getLengingInformationForUser(String IdOfUser){
		Connection con = null;
		Map<String, Vector> mapOfVector = new HashMap<String, Vector>();
		Vector data = new Vector();
		Vector<String> column = new Vector<String>();
		String sql = "select * from lending_"+IdOfUser;
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery(sql);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			for(int i=1;i<=rsMetaData.getColumnCount();i++) {
				column.add(rsMetaData.getColumnName(i));
			}
			while(rs.next()) {
				Vector row = new Vector();
				row.add(rs.getString(1));
				row.add(rs.getString(2));
				row.add(rs.getString(3));
				row.add(rs.getDate(4));
				row.add(rs.getInt(5));
				data.add(row);
			}
			sta.close();
			rs.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		mapOfVector.put("data", data);
		mapOfVector.put("column", column);
		return mapOfVector;
	}
	/**
	 * 该方法用于删除视图
	 * @param idOfUser
	 */
	public void dropTheView(String idOfUser) {
		Connection con = null;
		String sql = "drop view lending_"+idOfUser;
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			sta.execute(sql);
			sta.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
	}
	/**
	 * 该方法用于添加对应的用户账号信息
	 * @param con
	 * @param account 封装了对应账号信息的实体类
	 * @return 添加成功则返回true
	 */
	public boolean addAccount(Connection con,Account account) {
		String sql = "insert into account_information values(?,?,?,?,?)";
		int executeNum = 0;
		try {
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, account.getId());
			//如果用户没有输入昵称，则自动给出
			if(account.getName().equals("")) {
				pre.setString(2, "用户"+account.getId());
			}else {
			pre.setString(2, account.getName());
			}
			pre.setString(3, account.getGender());
			pre.setString(4, account.geteMail());
			pre.setLong(5, account.getPhone());
			executeNum = pre.executeUpdate();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 该方法用于删除表中对应的用户数据
	 * @param id	要删除的用户数据的ID
	 * @return
	 */
	public boolean delete(String id) {
		Connection con = null;
		String sql = "delete from account_information where id=?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, id);
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
	
}
