package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import model.LendingOfUSer;

import util.DBUtil;
public class LendingOfUserDao {
	/**
	 * 该方法用于更新图书归还日期
	 * 
	 */
	public void refreshDaysOfRetrun() {
		java.sql.Date dateOfBorrowed = null;
		int numberOfRenew = 0;
		Connection con = null;
		Statement sta = null;
		ResultSet rs = null;
		String sql = "select DATEOFBORROW,DATEOFRETURN,NUMBEROFRENEW from lending_information";
		try {
			con = DBUtil.getConnection();
			//获取结果集并激活游标模式，即游标可滚动且可以更改数据
			sta = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rs = sta.executeQuery(sql);
			while(rs.next()) {
				dateOfBorrowed = rs.getDate("DATEOFBORROW");
				numberOfRenew = rs.getInt("NUMBEROFRENEW");
				Date dateOfReturn = getReturnDate(dateOfBorrowed,numberOfRenew);
				rs.updateDate("DATEOFRETURN", dateOfReturn);
				rs.updateRow();
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				sta.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			DBUtil.closeConnection(con);
		}
	}
	/**
	 * 该方法用于计算出图书归还日期
	 * @param dateOfBorrowed 借书日期
	 * @param numberOfRenew	 续借次数
	 * @return 返回一个java.sql.Date对象，该对象封装了对应图书的归还日期
	 */
	private Date getReturnDate(Date dateOfBorrowed,int numberOfRenew) {
		//以借书日期为参数，获取一个对应的日历对象
		Calendar calendar = new Calendar.Builder().setInstant(dateOfBorrowed).build();
		if(numberOfRenew==0) {
			//如果没有续借过，日历往后翻三个月
			calendar.add(Calendar.MONTH, 3);
		}else if(numberOfRenew==1) {
			//如果有续借过1次
			calendar.add(Calendar.MONTH, 4);
		}else {
			//如果续借了两次
			calendar.add(Calendar.MONTH, 5);
		}
		//获取一个封装了归还日期的Date类
		java.util.Date uReturnDate = calendar.getTime();
		Date sReturnDate = new Date(uReturnDate.getTime());
		return sReturnDate;
	}
	/**
	 * 该方法用于查询所有借阅信息
	 * @return 返回一个包含两个向量的散列映射
	 */
	public Map<Integer, Vector> searchAll() {
		//刷新表单中借出天数的记录
		refreshDaysOfRetrun();
		Map<Integer,Vector> mapOfVector = new HashMap<Integer, Vector>();
		Vector<String> column = new Vector<String>();
		Vector data = new Vector();
		String sql = "select * from lending_information";
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery(sql);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			//遍历ResultSetMetaData以获得所有字段名
			for(int i=1;i<=rsMetaData.getColumnCount();i++) {
				column.add(rsMetaData.getColumnName(i));
			}
			while(rs.next()) {
				data.add(getRowVector(rs));
			}
			rs.close();
			sta.close();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		mapOfVector.put(1, data);
		mapOfVector.put(2, column);
		return mapOfVector;
	}
	/**
	 * 查询事件的处理
	 * @param string
	 * @return
	 */
	public Vector search(String string) {
		Connection con = null;
		Vector data = new Vector();
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery(string);
			while(rs.next()) {
				data.add(getRowVector(rs));
			}
			rs.close();
			sta.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		return data;
	
	}
	/**
	 * 把结果集按顺序封装到行向量中
	 * @param row
	 * @return
	 * @throws SQLException 
	 */
	private Vector getRowVector(ResultSet rs) throws SQLException {
		Vector row = new Vector();
		row.add(rs.getString(1));
		row.add(rs.getString(2));
		row.add(rs.getDate(3));
		row.add(rs.getLong(4));
		row.add(rs.getDate(5));
		return row;
	}
	/**
	 * 归还图书时对借书表相应的信息进行删除，对图书表相应的信息进行修改
	 * @param lendingOfUSer 借书表的实体类，其封装了对应的数据信息
	 */
	public boolean returnBook(LendingOfUSer lendingOfUSer) {
		Connection con = null;
		int executeNumInLending = 0;
		int executeNumInBook = 0;
		//该sql语句是对书记表的信息进行修改
		String sqlOfUpdate = "update book_information set BOOKCASE='在馆'  where BOOKNUMBER=?";
		//该sql语句是对借书表的信息进行删除
		String sqlDelete = "delete from lending_information where idOfBook=?";
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sqlDelete);
			pre.setString(1, lendingOfUSer.getIdOfBook());
			executeNumInLending = pre.executeUpdate();
			pre = con.prepareStatement(sqlOfUpdate);
			pre.setString(1, lendingOfUSer.getIdOfBook());
			executeNumInBook = pre.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNumInLending==1&&executeNumInBook==1) {
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 *	该方法用于添加借书信息,并把图书信息表对应的图书的状态改为借出。
	 * @param lendingOfUSer
	 * @return
	 */
	public boolean lendingBook(LendingOfUSer lendingOfUSer) {
			int executeNumInLending = 0;
			int executeNumInBook = 0;
			Connection con = null;
			String sql = "insert into lending_information(IDOFBORROWER,IDOFBOOK,DATEOFBORROW) values(?,?,?)";
			String changedBookCase = "update book_information set BOOKCASE =? where BOOKNUMBER=?";
			try {
				con = DBUtil.getConnection();
				PreparedStatement pre = con.prepareStatement(sql);
				pre.setString(1, lendingOfUSer.getIdOfBorrower());
				pre.setString(2, lendingOfUSer.getIdOfBook());
				pre.setDate(3, lendingOfUSer.getDateOfBorrowed());
				executeNumInLending = pre.executeUpdate();
				pre = con.prepareStatement(changedBookCase);
				pre.setString(1, "借出");
				pre.setString(2, lendingOfUSer.getIdOfBook());
				executeNumInBook = pre.executeUpdate();
				//填充还书日期
				refreshDaysOfRetrun();
			} catch (SQLException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}finally {
				DBUtil.closeConnection(con);
			}
			if(executeNumInLending==1&&executeNumInBook==1) {
				return true;
			}else {
				return false;
			}
		}
	
	
	/**
	 * 该方法用于判断输入的图书编号的真实性和借阅状况
	 * @param idOfBook
	 * @return
	 */
	public boolean exsitInBookDatabase(String idOfBook) {
		Connection con = null;
		//用于记录查询的结果条数
		int executeNum = 0;
		String bookCase = null;
		String sql = "select bookcase from book_information where BOOKNUMBER=?";
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, idOfBook);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				executeNum++;
				bookCase = rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		//只有当查询条数为1且该图书在馆时才返回true
		if(executeNum==1&&bookCase.equals("在馆")) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 该方法用于判断输入的借书人账号是否存在于用户信息表里
	 * @param lendingOfUSer
	 * @return
	 */
	public boolean exsitInUserDatabase(String idOfUser) {
		Connection con = null;
		//用于记录查询的结果条数
		int executeNum = 0;
		String sql = "select id from user_information where id=?";
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1,idOfUser);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				executeNum++;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 该方法用于处理图书续借
	 * @param lendingOfUSer 某一图书的具体信息
	 * @return 
	 */
	public boolean renew(LendingOfUSer lendingOfUSer) {
		Connection con = null;
		String sql = "update lending_information set NUMBEROFRENEW =? where IDOFBOOK=? ";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setInt(1, lendingOfUSer.getNumberOfRenew()+1);
			pre.setString(2, lendingOfUSer.getIdOfBook());
			executeNum = pre.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
		
	}
}
