package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import model.User;
import util.DBUtil;

public class UserDao {
	private static final String user_information = null;
	/**
	 * 该方法用于处理用户登录问题，若登录信息与数据库信息匹配。则返回实体类user
	 * @param con
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public User login(Connection con,User user) throws Exception{
		User result = null;
		//采用预处理语句进行查询
		String sql = "select password,role from user_information where id=?";
		String password = null;
		String role = null;
		PreparedStatement pre = con.prepareStatement(sql);
		//把从界面获取的信息填充到预处理语句中
		pre.setString(1, user.getId());
		//执行查询
		ResultSet rs = pre.executeQuery();
		while(rs.next()) {
			password = rs.getString("password");
			role = rs.getString("role");
		}
		//如果查到了对应记录，把结果集封装到result中以便后续区分身份时调用
		if(user.getPassword().equals(password)) {
			result = new User();
			result.setId(user.getId());
			result.setPassword(password);
			result.setRole(role);
		}
		rs.close();
		return result;
	}
	/**
	 * 该方法用于处理系统管理员向数据库增添新数据的问题
	 * @param con
	 * @param user
	 * @param numberOfExe 用于判断执行的sql语句插入了几条信息
	 * @return
	 * @throws SQLException 
	 * @throws Exception
	 */
	public boolean insertInformation(Connection con,User user ) throws SQLException{
		//k的值用于判断要插入的数据是否与数据库以有数据冲突
			int k = isOrNotInDatabase(con, user.getId());
			if(k==0) {
					String sql = "insert into user_information values(?,?,?)";
					PreparedStatement pre = con.prepareStatement(sql);
						//将user封装的信息填充到预处理语句中
						pre.setString(1, user.getId());
						pre.setString(2, user.getPassword());
						pre.setString(3, user.getRole());
						int numberOfExecute = pre.executeUpdate();
						pre.close();
						//DBUtil.closeConnection(con);
						if(numberOfExecute==1) {
							return true;
							}
						else {
							return false;
								}
						}	
			else 	{
				return false;
			}
		}
	/**
	 * 该方法和用于处理系统管理员删除用户的事件
	 * @param con
	 * @param user	从用户交互界面上获取的输入信息封装成的一个类
	 * @return
	 * @throws SQLException
	 */
	public boolean deleteUser(Connection con,User user) throws SQLException {
			//先判断要删除的账号在数据库中是否存在
			int k = isOrNotInDatabase(con, user.getId());
			if(k!=0) {
				String sql = "delete from user_information where id = ?";
				PreparedStatement pre = con.prepareStatement(sql);
				pre.setString(1, user.getId());
				int numberOfExecute = pre.executeUpdate();
				pre.close();
				//DBUtil.closeConnection(con);
				if(numberOfExecute==1) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
	/**
	 * 该方法用于处理修改数据库中的用户信息
	 * @param con
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public boolean changeUserInformation(Connection con,User user) throws SQLException {
		int k = isOrNotInDatabase(con, user.getId());
		//如果k不等于0，则证明数据库的表格中存在对应的账号信息
		if(k!=0) {
			String sql = "update user_information set password = ? where id = ?";
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, user.getPassword());
			pre.setString(2, user.getId());
			int numberOfExecute = pre.executeUpdate();
			pre.close();
			if(numberOfExecute==1) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	/**
	 * 该方法用于查询所有用户
	 * @return 返回一个包含字段名向量和数据向量的散列映射
	 */
	public Map<String, Vector> searchAllUser(){
		Map<String, Vector> mapOfVector = new HashMap<String, Vector>();
		Connection con = null;
		String sql = "select * from user_information where role in ('用户','图书管理员')";
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		Vector<String> column = new Vector<String>();
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery(sql);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			//读取表格的属性名
			for(int i=1;i<=rsMetaData.getColumnCount();i++) {
				column.add(rsMetaData.getColumnName(i));
			}
			while(rs.next()) {
				Vector<String> row = new Vector<String>();
				//通过遍历所读取行的所有列把数据装入row向量中
				for(int i=1;i<=rsMetaData.getColumnCount();i++) {
					row.add(rs.getString(i));
				}
				data.add(row);
			}
			//释放资源
			sta.close();
			rs.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		mapOfVector.put("data", data);
		mapOfVector.put("column", column);
		return mapOfVector;
		
	}
	/**
	 * 该方法用于查询用户
	 * @param id 要查询的用户账号
	 * @return
	 */
	public Vector<Vector<String>> searchUser(String id){
		Connection con = null;
		String sql = "select * from user_information where id = ?";
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1,id);
			ResultSet rs = pre.executeQuery();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			while(rs.next()) {
				Vector<String> row = new Vector<String>();
				for(int i=1;i<=rsMetaData.getColumnCount();i++) {
					row.add(rs.getString(i));
				}
				data.add(row);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return data;
	}
		
	/**
	 * 该方法用于判断数据是否存在对应的表中,若不在表中则返回0。
	 * @param
	 * @return
	 * @throws SQLException
	 */
	public int isOrNotInDatabase(Connection con,String id) throws SQLException {
		String string = "select * from user_information where id = ?";
		PreparedStatement presStatement = null;
		presStatement = con.prepareStatement(string);
		presStatement.setString(1, id);
		ResultSet rs = presStatement.executeQuery();
		int k = 0;
		if(rs.next()) {
			k++;
		}
		rs.close();
		return k;
	}
/**
 * 该方法用于删除对应账号的用户
 * @param id
 * @return
 */
	public boolean delect(String id) {
		Connection con = null;
		String sql = "delete from user_information where id = ?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, id);
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 用于单独修改登录密码
	 * @param user 封装了新密码和用户账号的实体类
	 * @return
	 */
	public boolean changePassword(User user) {
		Connection con = null;
		String sql = "update user_information set password=? where id=?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, user.getPassword());
			pre.setString(2, user.getId());
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
}

