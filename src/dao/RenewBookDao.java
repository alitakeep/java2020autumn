package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.PrimitiveIterator.OfDouble;

import model.LendingOfUSer;
import model.RenewBook;
import util.DBUtil;
/**
 * 该类是处理RenewBook实体与数据库连接的类
 * @author 10432
 *
 */
public class RenewBookDao {
	/**
	 * 该方法用于向续借信息表添加相应图书的续借信息
	 * @param renewBook 封装了对应用户ID和图书编号
	 * @author 10432
	 *@return 若以添加过信息，则返回-1；若添加成功，则返回1；若失败，则返回0
	 */
	public int setMessage(RenewBook renewBook) {
		if(!existInDatabase(renewBook.getIdOfBook())) {
			Connection con = null;
			String sql = "insert into renew_information(idofbook,id,reply) values(?,?,?)";
			int executeNum = 0;
			try {
				con = DBUtil.getConnection();
				PreparedStatement pre = con.prepareStatement(sql);
				pre.setString(1,renewBook.getIdOfBook());
				pre.setString(2, renewBook.getId());
				pre.setString(3, "null");
				executeNum = pre.executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
			}finally {
				DBUtil.closeConnection(con);
			}
			if(executeNum==1) {
				return 1;
			}else {
				return 0;
			}
		}else {
			return -1;
		}
	}
	/**
	 * 该方法用于删除表中的续借信息
	 * @param renewBook 封装了对应用户ID和图书编号
	 * @return
	 */
	public boolean delectMessage(RenewBook renewBook) {
		Connection con = null;
		String sql = "delete from renew_information where id=?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1,renewBook.getId());
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 该方法用于获取申请续借的用户的用户账号和图书编号
	 * @return 返回一个包含着两个向量的散列映射
	 */
	public Map<String, Vector> searchAll(){
		Map<String, Vector> mapOfVector = new HashMap<String, Vector>();
		Connection con = null;
		//只找出图书管理员还未审批的数据。
		String sql = "select idofbook,id from renew_information where reply='null' ";
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		Vector<String> column = new Vector<String>();
		try {
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			ResultSet rs = sta.executeQuery(sql);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			//获取属性名
			for(int i=1;i<=rsMetaData.getColumnCount();i++) {
				column.add(rsMetaData.getColumnName(i));
			}
			//由于该项是在别的数据库中查找，所以单独添加
			column.add("续借次数");
			while(rs.next()) {
				Vector<String> row = new Vector<String>();
				for(int i=1;i<=rsMetaData.getColumnCount();i++) {
					row.add(rs.getString(i));
					if(i==rsMetaData.getColumnCount()) {
						//在行向量的末尾添加该图书的续借次数
						row.add(String.valueOf(getNumberOfRenew(rs.getString("idOfBook"))));
					}
				}
				data.add(row);
			}
			sta.close();
			rs.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		mapOfVector.put("data", data);
		mapOfVector.put("column", column);
		return mapOfVector;
	}
	/**
	 * 同意续借图书时，修改续借表中相应的数据并同时修改借书表的续借次数
	 * @param renewBook
	 * @return
	 */
	public boolean agreeRenew(RenewBook renewBook) {
		Connection con = null;
		String sql = "update renew_information set reply=? where idofbook=?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, "成功");
			pre.setString(2, renewBook.getIdOfBook());
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			LendingOfUserDao dao = new LendingOfUserDao();
			//修改续借次数
			boolean resultOfChange = dao.renew(new LendingOfUSer(renewBook.getIdOfBook(), getNumberOfRenew(renewBook.getIdOfBook())));
			//刷新图书归还日期
			dao.refreshDaysOfRetrun();
			if(resultOfChange) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	/**
	 * 拒绝续借图书时，修改续借表中相应的数据
	 * @param renewBook
	 * @return
	 */
	public boolean refuseRenew(RenewBook renewBook) {
		Connection con = null;
		String sql = "update renew_information set reply=? where idofbook=?";
		int executeNum = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, "失败");
			pre.setString(2, renewBook.getIdOfBook());
			executeNum = pre.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(executeNum==1) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 查找相应用户的借阅申请信息
	 * @param id
	 * @return 返回一个包含数量不等的RenewBook实体类的向量
	 */
	public Vector<RenewBook>  seach (String id) {
		Connection con = null;
		String sql = "select idOfBook,reply from renew_information where id=?";
		Vector<RenewBook> vectorOfRenewBooks = new Vector<RenewBook>();
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, id);
			ResultSet rs = pre.executeQuery();
			//把查到的信息分别封装到向量中
			while(rs.next()) {
				RenewBook renewBook = new RenewBook();
				//获取图书名称
				String bookName = getBookName(rs.getString("idOfBook"));
				renewBook.setBookName(bookName);
				renewBook.setReply(rs.getString("reply"));
				vectorOfRenewBooks.add(renewBook);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return vectorOfRenewBooks;
	}
	/**
	 * 
	 * @param idOfBook
	 * @return 若某一图书申请借阅且管理员尚未审批，则返回true，否则返回false
	 */
	public boolean existInDatabase(String idOfBook) {
		Connection con = null;
		String sql = "select * from renew_information where idofbook=? and reply=?";
		int numberOfData = 0;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, idOfBook);
			pre.setString(2, "null");
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				numberOfData++;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		if(numberOfData>0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 该方法可获取对应书籍编号的书籍名称
	 * @param idOfBook 对应的书籍编号
	 * @return 书籍名称
	 */
	public String getBookName(String idOfBook) {
		Connection con = null;
		String sql = "select bookName from book_information where bookNumber=?";
		String bookName = null;
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, idOfBook);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				bookName = rs.getString("bookName");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return bookName;
	}
	/**
	 * 该方法可以获取对应书籍编号的续借次数
	 * @param idOfBook 对应的书籍编号
	 * @return 对应书籍的图书编号
	 */
	public int getNumberOfRenew(String idOfBook) {
		int numberOfRenew = -1;
		Connection con = null;
		String sql = "select NUMBEROFRENEW from lending_information where idOfBook=?";
		try {
			con = DBUtil.getConnection();
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, idOfBook);
			ResultSet rs = pre.executeQuery();
			while(rs.next()) {
				numberOfRenew = rs.getInt(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			DBUtil.closeConnection(con);
		}
		return numberOfRenew;
	}
}
