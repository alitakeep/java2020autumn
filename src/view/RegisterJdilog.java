package view;

import java.awt.BorderLayout;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



import dao.AccountDao;
import dao.UserDao;
import model.Account;
import model.User;
import util.ChangeBackground;
import util.DBUtil;
import util.IsTextEmptyOrNot;
import util.RoundButton;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JPasswordField;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.UIManager;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;
import java.awt.event.ActionEvent;

public class RegisterJdilog extends JDialog {

	private final JPanel contentPanel ;
	private JTextField idTxt;
	private JTextField nameTxt;
	private JTextField emailTxt;
	private JPasswordField passwordField;
	private JPasswordField confirmPasswordField;
	private JTextField phoneTxt;
	private JComboBox genderCBox;
	private JLabel lblNewLabel_5_2;
	
	private UserDao userDao = new UserDao();
	private AccountDao accountDao = new AccountDao();


	/**
	 * Create the dialog.
	 */
	public RegisterJdilog() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(RegisterJdilog.class.getResource("/picture/\u6CE8\u518C\u6807\u9898.png")));
		contentPanel = new ChangeBackground(new ImageIcon(getClass().getResource("/picture/天气之子.jpg")));
		setBounds(100, 100, 597, 431);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		idTxt = new JTextField();
		idTxt.setBounds(116, 52, 103, 21);
		contentPanel.add(idTxt);
		idTxt.setColumns(10);
		
		nameTxt = new JTextField();
		nameTxt.setColumns(10);
		nameTxt.setBounds(116, 174, 103, 21);
		contentPanel.add(nameTxt);
		
		emailTxt = new JTextField();
		emailTxt.setColumns(10);
		emailTxt.setBounds(116, 217, 103, 21);
		contentPanel.add(emailTxt);
		
		JLabel lblNewLabel = new JLabel("\u6CE8\u518C\u8D26\u53F7\uFF1A");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(31, 49, 84, 20);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u5BC6\u7801\uFF1A");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(57, 98, 103, 15);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(38, 135, 103, 15);
		contentPanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u8D26\u53F7\u6635\u79F0\uFF1A");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_3.setBounds(42, 177, 103, 15);
		contentPanel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("\u90AE\u7BB1\u5730\u5740\uFF1A");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_4.setBounds(37, 219, 103, 15);
		contentPanel.add(lblNewLabel_4);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(116, 93, 103, 21);
		contentPanel.add(passwordField);
		
		confirmPasswordField = new JPasswordField();
		confirmPasswordField.setBounds(116, 132, 103, 21);
		contentPanel.add(confirmPasswordField);
		
		phoneTxt = new JTextField();
		phoneTxt.setColumns(10);
		phoneTxt.setBounds(116, 263, 103, 21);
		contentPanel.add(phoneTxt);
		
		JLabel lblNewLabel_5_1 = new JLabel("\u624B\u673A\u53F7\u7801\uFF1A");
		lblNewLabel_5_1.setForeground(Color.WHITE);
		lblNewLabel_5_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_5_1.setBounds(31, 264, 103, 15);
		contentPanel.add(lblNewLabel_5_1);
		
		lblNewLabel_5_2 = new JLabel("\u6027\u522B:");
		lblNewLabel_5_2.setForeground(Color.WHITE);
		lblNewLabel_5_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_5_2.setBounds(66, 311, 49, 15);
		contentPanel.add(lblNewLabel_5_2);
		
		genderCBox = new JComboBox();
		genderCBox.setModel(new DefaultComboBoxModel(new String[] {"\u7537", "\u5973"}));
		genderCBox.setBounds(116, 309, 44, 23);
		//默认性别为男
		genderCBox.setSelectedIndex(0);
		contentPanel.add(genderCBox);
		
		JButton confirmButton = new RoundButton("确认",new Color(248, 248, 255) ,new Color(135,206,250));
		//JButton confirmButton = new JButton("\u6CE8\u518C");
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterActionPerformed();
			}
		});
		confirmButton.setBackground(Color.WHITE);
		confirmButton.setIcon(new ImageIcon(RegisterJdilog.class.getResource("/picture/\u6CE8\u518C\u754C\u9762.png")));
		confirmButton.setForeground(UIManager.getColor("Button.disabledShadow"));
		confirmButton.setBounds(279, 292, 84, 26);
		contentPanel.add(confirmButton);
	}
	/**
	 * 处理注册事件
	 */
	private void RegisterActionPerformed() {
		String firstPassword = new String(passwordField.getPassword());
		String secondPassword = new String(confirmPasswordField.getPassword());
		IsTextEmptyOrNot isTextEmptyOrNot = new IsTextEmptyOrNot();
		//判断账号是否为空
		boolean haveId = isTextEmptyOrNot.isTheTextEmptyOrNot(idTxt, "账号");
		if(passwordField.getPassword()==null) {
			JOptionPane.showMessageDialog(null, "密码不能为空", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/难过.png")));
		}else if(passwordField.getPassword()==null) {
			JOptionPane.showMessageDialog(null, "请确认密码", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/难过.png")));
		}
		//判断两次输入的密码是否相同
		else if(!firstPassword.equals(secondPassword)) {
		JOptionPane.showMessageDialog(null, "两次输入的密码不一致", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/难过.png")));
		}else if(!haveRightPhone(phoneTxt.getText())) {
		JOptionPane.showMessageDialog(null, "请输入正确的手机号", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/难过.png")));
		}//如果输入的内容符合所有规则且账号不为空，则进行注册
		else if(haveId) {
			//把相应的信息封装到对应的实体类中
			User user = packInUser(firstPassword);
			Account	account = packInAccount();
			boolean addInUser = false;
			boolean addInAccount = false;
			Connection con = null;
			try {
				 con = DBUtil.getConnection();
				addInUser = userDao.insertInformation(con, user);
				//如果用户能添加成功，再把对应的信息添加进数据库
				if(addInUser) {
				 addInAccount = accountDao.addAccount(con, account);
				}
			} catch (SQLException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}finally {
				DBUtil.closeConnection(con);
			}
			//如果添加成功，弹出提示框
			if(addInAccount&&addInUser) {
				JOptionPane.showMessageDialog(null, "注册成功！", "提示信息", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/笑脸.png")));
				RegisterJdilog.this.dispose();
			}else {
				JOptionPane.showMessageDialog(null, "注册失败！", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/难过.png")));
			}
		}
	}
	
	/**
	 * 该方法用于判断是否输入了正确的电话号码
	 * @param phone 文本框中输入的信息
	 * @return 如果包含除了数字以外的字符，返回false。如果格式正确，返回true
	 */
	private boolean haveRightPhone(String phone) {
		Scanner scanner = new Scanner(phone);
		scanner.useDelimiter("[\\d]+");
		if(scanner.hasNext()) {
			scanner.close();
			return false;
		}else {
			scanner.close();
			return true;
		}
	}
	private User packInUser(String password) {
		User user = new User();
		user.setId(idTxt.getText());
		user.setPassword(password);
		user.setRole("用户");
		return user;
	}
	private Account packInAccount() {
		Account account = new Account();
		account.setId(idTxt.getText());
		account.setName(nameTxt.getText());
		account.seteMail(emailTxt.getText());
		//如果填写了手机号，则把手机号也封装进Account类中
		if(!phoneTxt.getText().equals("")) {
		account.setPhone(Long.parseLong(phoneTxt.getText()));
		}
		account.setGender(genderCBox.getSelectedItem().toString());
		return account;
	}
}
