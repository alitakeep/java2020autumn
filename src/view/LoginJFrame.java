package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


import dao.UserDao;
import model.User;
import util.ChangeBackground;
import util.DBUtil;
import util.RoundButton;
import viewBookManage.BookManageFrame;
import viewManage.ManageFrame;
import viewUser.UserJFrame;

public class LoginJFrame extends JFrame {

	private JPanel contentPane;
	private JTextField userNameTxt;
	private JPasswordField passwordTxt;
	
	private UserDao userDao = new UserDao();
	/**
	 * Create the frame.
	 */
	public LoginJFrame() {
		setResizable(false);
		setType(Type.UTILITY);
		setTitle("\u767B\u5F55\u754C\u9762\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 932, 403);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(231, 221, 203));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u56FE\u4E66\u7BA1\u7406\u7CFB\u7EDF");
		lblNewLabel.setIcon(new ImageIcon(LoginJFrame.class.getResource("/picture/\u56FE\u4E66.png")));
		lblNewLabel.setFont(new Font("宋体", Font.BOLD, 30));
		lblNewLabel.setBounds(580, 37, 315, 100);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u8D26\u53F7\r\n\r\n");
		lblNewLabel_1.setEnabled(false);
		lblNewLabel_1.setBackground(new Color(176, 224, 230));
		lblNewLabel_1.setFont(new Font("宋体", Font.BOLD, 20));
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setBounds(583, 170, 81, 35);
		contentPane.add(lblNewLabel_1);
		
		userNameTxt = new JTextField();
		userNameTxt.setBounds(659, 170, 222, 35);
		contentPane.add(userNameTxt);
		userNameTxt.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("\u5BC6\u7801\r\n");
		lblNewLabel_2.setEnabled(false);
		lblNewLabel_2.setBackground(new Color(176, 224, 230));
		lblNewLabel_2.setFont(new Font("宋体", Font.BOLD, 20));
		lblNewLabel_2.setBounds(583, 225, 81, 24);
		contentPane.add(lblNewLabel_2);
		passwordTxt = new JPasswordField();
		passwordTxt.setBounds(659, 220, 222, 35);
		contentPane.add(passwordTxt);
		
		JButton loginButton = new RoundButton("登录", 2);
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					loginActionPerformed(e);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		loginButton.setBackground(new Color(176, 224, 230));
		loginButton.setFont(new Font("宋体", Font.PLAIN, 12));
		loginButton.setForeground(new Color(0, 0, 0));
		loginButton.setIcon(new ImageIcon(LoginJFrame.class.getResource("/picture/\u767B\u5F55.png")));
		loginButton.setBounds(791, 298, 90, 35);
		contentPane.add(loginButton);
		//设置登录按钮的快捷键为回车键
		getRootPane().setDefaultButton(loginButton);
		
		JButton registerButton = new RoundButton("注册", 0);
		//JButton registerButton = new JButton("  \u6CE8\u518C\r\n");
		registerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new RegisterJdilog().setVisible(true);
			}
		});
		registerButton.setIcon(new ImageIcon(LoginJFrame.class.getResource("/picture/\u6CE8\u518C.png")));
		registerButton.setFont(new Font("宋体", Font.PLAIN, 12));
		registerButton.setBackground(new Color(176, 224, 230));
		registerButton.setBounds(659, 298, 90, 35);
		contentPane.add(registerButton);
		
		JPanel photoJPanel = setPhotoOnJPanel();
		photoJPanel.setBounds(0, 0, 570, 376);
		contentPane.add(photoJPanel);
		//设置窗体居中
		this.setLocationRelativeTo(null);
		//创建数据库
		creatDataBase();
	}
	
	/**
	 * 放置图片以美化登录界面
	 * @param photoJPanel
	 */
private JPanel setPhotoOnJPanel() {
		ImageIcon icon = new ImageIcon(getClass().getResource("/picture/圣三一.jpg"));
		JPanel photoJPanel = new ChangeBackground(icon);
		return photoJPanel;
	}
/**
 * 登录事件的处理
 * @param e
 * @throws Exception 
 */
	private void loginActionPerformed(ActionEvent e) throws Exception {
		String userName = this.userNameTxt.getText();					//获取输入框中的账号
		String password = new String(this.passwordTxt.getPassword());	//获取输入框中的密码
		User user = new User(userName,password);						//把界面信息封装到user类中
		Connection conn = null;
		try {
			conn = DBUtil.getConnection();
			User judgeUser = userDao.login(conn, user);					//根据login函数返回的user类的引用判断是否登录成功
			DBUtil.closeConnection(conn);
			if(judgeUser!=null) {
				this.dispose();											//若登录成功，则销毁当前窗体
				if(judgeUser.getRole().equals("系统管理员")) {				//进行身份区分，不同身份对应不同的操作界面
					new ManageFrame().setVisible(true);
				}
				else if(judgeUser.getRole().equals("图书管理员")) {
					new BookManageFrame().setVisible(true);
				}
				else if(judgeUser.getRole().equals("用户")) {
					new UserJFrame(judgeUser).setVisible(true);
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "用户名或密码错误");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		
		}
	}
	private void creatDataBase() {
		Connection con = null;
		try {
			//登录表
			String sql1 = "create table user_information(id varchar(20) primary key, password varchar(20), role varchar(10))";
			String sql2 = "insert into user_information values('20001','admin','系统管理员')";
			String sql3 = "insert into user_information values('21001','Aa123','图书管理员')";
			String sql4 = "insert into user_information values('22001','123','用户')";
			
			//图书信息表
			String sql5 = "create table book_information(BOOKNUMBER varchar(15) primary key, BOOKNAME varchar(20), BOOKTYPE varchar(10),BOOKCASE varchar(4),AUTHOR varchar(20),INTRODUCTION CLOB, PICTURE Blob)";
			
			//用户账号信息表
			String sql6 = "create table account_information(ID varchar(20) primary key, NAME varchar(10), GENDER varchar(4), email varchar(30), phone bigint)";
			String sql7 = "insert into account_information values('22001','Alita','男', '0123@scau.com', 12345678910)";
			
			//借阅信息表
			String sql8 = "create table lending_information(IDOFBORROWER varchar(20), IDOFBOOK varchar(20),primary key(IDOFBORROWER,IDOFBOOK),DATEOFBORROW Date,NUMBEROFRENEW int,DATEOFRETURN Date )";
			
			//创建续借信息表
			String sql9 = "create table renew_information (IDOFBOOK varchar(20), ID varchar(10), primary key(IDOFBOOK,ID),REPLY varchar(5))";
			con = DBUtil.getConnection();
			Statement sta = con.createStatement();
			sta.execute(sql1);
			sta.execute(sql2);
			sta.execute(sql3);
			sta.execute(sql4);
			sta.execute(sql5);
			sta.execute(sql6);
			sta.execute(sql7);
			sta.execute(sql8);
			sta.execute(sql9);
			
		} catch (Exception e) {
		}finally {
			DBUtil.closeConnection(con);
		}
	
	}
}
