package util;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
/**
 * 该类重写了JPanel类中的paintComponent(Graphics g)方法，用于绘制窗口背景
 * @author 10432
 *
 */
public class ChangeBackground extends JPanel {
	ImageIcon imageIcon = null;
	
	public ChangeBackground() {
		
	}
	public ChangeBackground(ImageIcon imageIcon) {
		super();
		this.imageIcon = imageIcon;
		this.setLayout(new BorderLayout());
	}

	@Override 
	/**
	 * 覆盖paintComponent方法重新绘制容器背景
	 */
	protected void paintComponent(Graphics g) {
		// TODO 自动生成的方法存根
		super.paintComponent(g);
		//设置背景图片随着窗口大小的变化一起改变
		g.drawImage(imageIcon.getImage(), 0, 0, this.getWidth(),this.getHeight(),this);
	}
	
	
}
