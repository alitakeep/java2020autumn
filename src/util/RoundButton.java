package util;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JButton;
/**
 * 该类继承JButton类，用于修改按钮的外观
 * @author 10432
 *
 */
public class RoundButton extends JButton{
	//用于标识按钮要填充的颜色
	  private int whichOne = -1;
	  private Color isArmed;
	  private Color noArmed;
	 /**
	  * 继承了JButton用于设置内容的构造方法，并且通过whichOne的值选择按钮的颜色
	  * @param title 按钮的内容
	  * @param whichOne 仅限0、1、2。用于标识按钮颜色
	  */
	  public RoundButton(String title,int whichOne) {
		super(title);
		//透明化该按钮，防止自定义的圆角边框和默认画出的矩形区域重叠
		setContentAreaFilled(false);
		//去除边框
		setBorderPainted(false);
		//去除虚线边框
		setFocusPainted(false);
		this.whichOne = whichOne;
	}
	 /**
	  * 该构造方法继承了JButton用于设置内容的构造方法，并且可自定义颜色
	  * @param title	按钮上的文字
	  * @param isArmed	按钮按下时的颜色
	  * @param noArmed	按钮默认颜色
	  */
	  public RoundButton(String title,Color isArmed, Color noArmed) {
		super(title);
		this.isArmed = isArmed;
		this.noArmed = noArmed;
		//透明化该按钮，防止自定义的圆角边框和默认画出的矩形区域重叠
		setContentAreaFilled(false);
		//去除边框
		setBorderPainted(false);
		//去除虚线边框
		setFocusPainted(false);
	}

	//自定义颜色便于填充
	  public static Color GroupOne_noArmed = new Color(252, 157, 154);
	  public static Color GroupOne_isArmed = new Color(255, 245,247);
	  
	  public static Color GroupTwo_noArmed = new Color(232,221,203);
	  public static Color GroupTwo_isArmed = new Color(196,226,216);
	  
	  public static Color GroupThree_noArmed = new Color(249, 205, 173);
	  public static Color GroupThree_isArmed = new Color(220, 220, 169);
	  
	  
	  @Override
	  /**
	   * 用指定颜色填充该圆角按钮
	   */
	protected void paintComponent(Graphics g) {
		  //如果whichOne的值有被修改，则按照该类中定义的颜色予以填充
		  if(whichOne!=-1) {
			  if(whichOne==0) {
				  if(getModel().isArmed()) {
					//如果按钮被选中，用自定义的颜色组填充
					  g.setColor(GroupOne_isArmed);
				  }else {
					//否则用定义的颜色填充
					  g.setColor(GroupOne_noArmed);
				  }
			  }else if(whichOne==1) {
				  if(getModel().isArmed()) {
						  g.setColor(GroupTwo_isArmed);
				  }else {
						  g.setColor(GroupTwo_noArmed);
					 }
			  }else if(whichOne==2) {
				  if(getModel().isArmed()) {
					  g.setColor(GroupThree_isArmed);
				  }else {
					  g.setColor(GroupThree_noArmed);
				  }
			  }
		  }else {
			  if(getModel().isArmed()) {
				  g.setColor(isArmed);
			  }else {
				g.setColor(noArmed);
			}
		  }
		g.fillRoundRect(0, 0, getSize().width-1,getSize().height-1,20, 20);
		super.paintComponent(g);
	}
	  @Override
	/**
	 * 绘制一个圆角的按钮边框
	 */
	protected void paintBorder(Graphics g) {
		//该方法的后两个参数从左到右分别代表着弧线的水平直径和弧线的垂直直径
		g.drawRoundRect(0,0,getSize().width-1,getSize().height-1, 20, 20);
		// TODO 自动生成的方法存根
		 super.paintBorder(g);
	}
}

