package util;

import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class OutLog {
	/**
	 * 退出登录事件的处理
	 * @throws SQLException 
	 */
	public void outLogActionPerform(JFrame ManageFrame) {
		JFrame jframe = ManageFrame;
		//先生成一个确认窗口防止用户误点
		int result = JOptionPane.showConfirmDialog(null, "是否退出系统");
		//如果用户点击“是”，则断开所有数据库并则销毁当前窗体
		if(result==0) {													
				/*try {
					Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
					DriverManager.getConnection("jdbc:derby:;shutdown=true");
				} catch (ClassNotFoundException evt) {
					// TODO 自动生成的 catch 块
					evt.printStackTrace();
				} catch (SQLException evt) {
					// TODO 自动生成的 catch 块
					evt.printStackTrace();
				}*/
				jframe.dispose();
		}
	}

}
