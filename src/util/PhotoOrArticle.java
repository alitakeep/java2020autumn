package util;


import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;

import java.sql.SQLException;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;
import javax.sql.rowset.serial.SerialException;
import javax.swing.ImageIcon;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import view.RegisterJdilog;
/**
 * 
 * @author 10432
 *
 */

public class PhotoOrArticle {
	//用于封装图片
	private SerialBlob blob;
	//用于封装书籍简介
	private SerialClob clob;
	/**
	 * 该方法生成一个文件选择器，在用户选择对应的图片之后把图片封装成SerialBlob类。以便后续写入数据库
	 * @return blob 包含有相应图片信息的实现了Blob接口的类
	 */
	public SerialBlob setPhoto() {
		//用于读取文件
		FileInputStream fIn = null;
		//实例化一个文件选择器
		JFileChooser chooser = new JFileChooser();
		//设置文件格式
		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & PNG Images","jpg", "png");
		//过滤文件
		chooser.setFileFilter(filter);
		int result = chooser.showOpenDialog(null);
		if(result == JFileChooser.APPROVE_OPTION) {
			File photoFile = chooser.getSelectedFile();
			try {
				fIn = new FileInputStream(photoFile);
				byte[] photo = new byte[fIn.available()];
				fIn.read(photo);
				blob = new SerialBlob(photo);
			} catch ( IOException | SQLException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}else {
			JOptionPane.showMessageDialog(null, "请上传正确的图片！", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(RegisterJdilog.class.getResource("/picture/难过.png")));
		}
		return blob ;
	}
	/**
	 * 将上传的图片封装成ImageIcon类
	 * @param blob 包含了某一图片信息的实现了Blob接口的类。
	 * @return	返回一个封装有该图片信息的ImageIcon类
	 */
	public ImageIcon setImage(SerialBlob blob) {
		ImageIcon icon = null;
		try {
			//将其转化成字节数组
			byte[] photo = blob.getBytes(1L,(int) blob.length());
			icon = new ImageIcon(photo);
			//设置大小
			icon.getImage().getScaledInstance(300, 260, Image.SCALE_DEFAULT);
		} catch (SerialException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return icon;
	}
	/**
	 * 将字符串转化为SerialCLob类
	 * @param text 图书内容简介
	 * @return	返回一个包含相应图书内容的实现了Clob接口的类。
	 */
	public SerialClob getClob(String text) {
		clob = null;
		try {
			clob = new SerialClob(text.toCharArray());
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return clob;
	}
	/**
	 * 该方法将SerialClob类型的数据转化成字符串输出
	 * @param clob	包含了对应信息的图书内容简介
	 * @return 返回包含了图书内容简介的字符串
	 */
	public String getArticle(SerialClob clob) {
		String text = null;
		try {
			text = clob.getSubString(1L,(int) clob.length());
		} catch (SerialException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return text;
	}
}
