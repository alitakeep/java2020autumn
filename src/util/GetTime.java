package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * 该类用于实时更新用户主界面上的时间和界面标题
 * @author 10432
 *
 */
public class GetTime extends Thread {
	private JFrame userFrame;
	private  volatile String name;
	private JLabel jLabel;
	/**
	 * 
	 * @param userFrame 相应窗体的引用
	 * @param name	用户名称
	 * @param jLabel 用于展示的标签的引用
	 */
	public GetTime(JFrame userFrame, String name, JLabel jLabel) {
		super();
		this.userFrame = userFrame;
		this.name = name;
		this.jLabel = jLabel;
	}
	/**
	 * 修改昵称
	 * @param newName 
	 */
	public synchronized void changeName(String newName) {
		name = newName;
	}
	@Override
	public void run() {
		while(true) {
			try {
				//休眠一秒
				Thread.sleep(1000);
				//获取当前时间
				Date nowDay = new Date();
				SimpleDateFormat textInLable = new SimpleDateFormat("'今天是'M'月'd'日'  EEE   HH:mm:ss");
				//以对应的时间格式填充到标签中
				jLabel.setText(textInLable.format(nowDay));
				Calendar nowTime = Calendar.getInstance();
				//获取当前时分
				int hour = nowTime.get(Calendar.HOUR_OF_DAY);
				//根据不同的时间设置标题内容
				if(hour>=6&&hour<12) {
					userFrame.setTitle("早上好，"+name);
				}else if(hour>=12&&hour<18) {
					userFrame.setTitle("下午好，"+name);
				}else if(hour>=18&&hour<24) {
					userFrame.setTitle("晚上好，"+name);
				}else {
					userFrame.setTitle("夜深了，"+name+"。要注意休息哦~");
				}
			} catch (InterruptedException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
	}
	
}
