package util;



import javax.swing.JComboBox;



/**
 *该类实现日期选择下拉列表和日期显示框的交互
 * @author 10432
 *
 */
public class GetDateFromJCombox {
	private JComboBox monthBox;
	private JComboBox dayBox;
	String date;
	
	public GetDateFromJCombox(JComboBox monthBox, JComboBox dayBox, String date) {
		super();
		this.monthBox = monthBox;
		this.dayBox = dayBox;
		this.date = date;
	}
	StringBuffer dateInText = new StringBuffer();
	/**
	 * 该方法用于修改文本框中对应的日期信息。
	 * @return 返回修改后的日期信息
	 */
	public String getSelectedDate() {
		dateInText.append(date);
		//日期的形式是固定不变的，都是以-为分界符号。所以只要获取两个-号所在的位置，便能准确修改月份和天数
		int firstAppear = dateInText.indexOf("-");
		int lastAppear = dateInText.lastIndexOf("-");
		//由于下拉框中的内容还包括文字，所以需要去掉文字内容，保留数字。
		String resetMonth = getNumberOfDate(monthBox.getSelectedItem().toString());
		String resetDay = getNumberOfDate(dayBox.getSelectedItem().toString());
		dateInText.replace(firstAppear+1, lastAppear,resetMonth);
		dateInText.replace(lastAppear+1, dateInText.length()+1,resetDay);
		return dateInText.toString();
	}
	/**
	 * 由于下拉列表中日期的形式都是数字加文字，且文字都在数字后面。所以只需要截取出文字之前的字符串即能得到所选的日期信息。
	 * @param date
	 * @return
	 */
	private String getNumberOfDate(String date) {
		String resetDate = date.substring(0, date.length()-1);
		//如果获取的月份或日期是单位数，则在其前面加个零
		if(resetDate.length()==1) {
			StringBuffer newResetDate = new StringBuffer("0");
			newResetDate.append(resetDate);
			return newResetDate.toString();
		}else {
			return resetDate;
		}
		
	}
}
