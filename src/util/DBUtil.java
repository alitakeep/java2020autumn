package util;
import java.sql.*;
/**
 * 连接数据库
 * @author 10432
 *
 */
public class DBUtil {
	private static  String url = "jdbc:derby:User;create=true";
	private static  String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	static Connection conn ;
	/**
	 * 获取数据库的连接
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	/**
	 * 关闭数据库的连接
	 * @param con 
	 */
	public static void closeConnection(Connection con) {
			try {
				if(con!=null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	/**
	 * 用于测试数据库是否能连接成功
	 */
	/*public static void main(String[] args) {
		try {
			Connection con = null;
			con = DBUtil.getConnection();
			System.out.println("数据库连接成功");
			DBUtil.closeConnection(con);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("数据库连接失败");
		}
		
	}*/
}

