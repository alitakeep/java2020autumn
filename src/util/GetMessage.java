package util;

import java.util.Vector;

import model.RenewBook;

public class GetMessage {
	/**
	 * 该方法将有关用户的申请信息改写成句子
	 * @param renewBooks 一个包含着若干个renewBook类的向量
	 * @return 返回一个包含着若干条信息的StringBuffer的数组
	 */
	public static StringBuffer[] getMessage(Vector<RenewBook> renewBooks) {
		//获取信息条数
		int count = renewBooks.size();
		//声音一个长度为count+1的StringBUffer数组
		StringBuffer[] message = new StringBuffer[count];
		for(int i=0;i<count;i++) {
			//如果管理员以审批该申请，则将起添加到message中
			if(!renewBooks.get(i).getReply().equals("null")) {
				message[i] = new StringBuffer();
				message[i].append("图书"+'《'+renewBooks.get(i).getBookName()+'》');
				message[i].append("续借"+renewBooks.get(i).getReply());
			}else {
				//如果管理员还未处理该信息，则直接实例化一个没有内容的message
				message[i] = new StringBuffer();
			}
		}
		return message;
	}
}
