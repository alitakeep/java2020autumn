package util;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
/**
 * 该类用于判断用户输入内容是否为空
 * @author 10432
 *
 */
public class IsTextEmptyOrNot {
	/**
	 * 
	 * @param jTextField
	 * @param JTextFieldName
	 * @return 如果为空，则返回false，且弹出提示窗口。否则返回true。
	 */
	public  boolean isTheTextEmptyOrNot(JTextField jTextField,String JTextFieldName) {
		if(jTextField.getText().trim().equals("")) {
		JOptionPane.showMessageDialog(null, JTextFieldName+"不能为空");
		return false;
		}
		else {
			return true;
		}
	}
	/**
	 * 
	 * @param jTextField
	 * @return 如果为空，则返回false，否则返回true
	 */
	public  boolean isTheTextEmptyOrNot(JTextField jTextField) {
		if(jTextField.getText().trim().equals("")) {
			return false;
			}
		else {
			return true;
		}
	}
	
}
