package util;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;


import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
/**
 * 该类继承了鼠标监听器类，是通过循环遍历表格的所有列把所选行的数据放置到对应的文本框或标签中
 * 该类必须传入包含有若干个和表格属性顺序和数量相对应的文本框或标签的向量
 * @author 10432
 *
 */
public class PackagingTabletextInTextField extends MouseAdapter {
	private JTable table;
	//该向量中包含着数量不等的JTextField的引用
	private Vector<JTextField> vectorOfFields;
	//该向量中包含着数量不等的JLable的引用
	private Vector<JLabel> vectorOfLabels;
	private Vector<JComponent> vector;
	public PackagingTabletextInTextField(JTable table,Vector<JTextField> vectorOfFields) {
		this.table = table;
		this.vectorOfFields = vectorOfFields;
	}
	public PackagingTabletextInTextField(Vector<JLabel> vectorOfLabels,JTable table) {
		super();
		this.table = table;
		this.vectorOfLabels = vectorOfLabels;
		//实例化该向量用于鼠标点击事件的判断
		vectorOfFields = new Vector<JTextField>();
	}

	/**
	 * 该方法通过遍历向量，把数据对应的放在文本框或标签中
	 */
	public void mouseClicked(MouseEvent e) {
		if(!vectorOfFields.isEmpty()) {
			for(int i=0;i<vectorOfFields.size();i++) {
				vectorOfFields.get(i).setText(table.getValueAt(table.getSelectedRow(), i).toString());
			}
		}else {
			for(int i=0;i<vectorOfLabels.size();i++) {
				vectorOfLabels.get(i).setText(table.getValueAt(table.getSelectedRow(), i).toString());
			}
		}
		
	}
	
}
