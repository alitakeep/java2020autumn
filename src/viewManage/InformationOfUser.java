package viewManage;



import javax.swing.JButton;
import javax.swing.JDialog;

import dao.AccountDao;
import model.Account;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InformationOfUser extends JDialog {
	private JTextField nameTxt;
	private JTextField genderTxt;
	private JTextField emailTxt;
	private JTextField phoneTxt;
	private AccountDao dao = new AccountDao();

	/**
	 * Create the dialog.
	 */
	public InformationOfUser(Account account) {
		setBounds(100, 100, 441, 321);
		getContentPane().setLayout(null);
		
		nameTxt = new JTextField();
		nameTxt.setBounds(95, 54, 141, 21);
		getContentPane().add(nameTxt);
		nameTxt.setColumns(10);
		
		genderTxt = new JTextField();
		genderTxt.setEditable(false);
		genderTxt.setColumns(10);
		genderTxt.setBounds(95, 110, 53, 21);
		getContentPane().add(genderTxt);
		
		emailTxt = new JTextField();
		emailTxt.setColumns(10);
		emailTxt.setBounds(95, 165, 141, 21);
		getContentPane().add(emailTxt);
		
		phoneTxt = new JTextField();
		phoneTxt.setColumns(10);
		phoneTxt.setBounds(95, 221, 141, 21);
		getContentPane().add(phoneTxt);
		
		JLabel lblNewLabel = new JLabel("\u6635\u79F0\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(35, 55, 75, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u6027\u522B\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(35, 111, 75, 15);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u90AE\u7BB1\u5730\u5740\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(10, 168, 81, 15);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u624B\u673A\u53F7\u7801\uFF1A");
		lblNewLabel_3.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_3.setBounds(10, 224, 88, 15);
		getContentPane().add(lblNewLabel_3);
		
		JButton changeButton = new JButton("\u4FEE\u6539");
		//为修改按钮添加监听器
		changeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeAction(account);
			}
		});
		changeButton.setBackground(SystemColor.controlLtHighlight);
		changeButton.setIcon(new ImageIcon(InformationOfUser.class.getResource("/picture/\u4FEE\u6539.png")));
		changeButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		changeButton.setBounds(287, 109, 93, 27);
		getContentPane().add(changeButton);
		
		JButton resetButton = new JButton("\u91CD\u7F6E");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fillText(account);
			}
		});
		resetButton.setBackground(SystemColor.controlLtHighlight);
		resetButton.setIcon(new ImageIcon(InformationOfUser.class.getResource("/picture/\u5237\u65B0.png")));
		resetButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		resetButton.setBounds(291, 164, 93, 27);
		getContentPane().add(resetButton);
		//填充对话框中的文本框
		fillText(account);
	}
	
	private void changeAction(Account account) {
		Account newAccount = new Account();
		//用于标志修改操作是否正确
		boolean correctOperation = true;
		try {
			Long.parseLong(phoneTxt.getText());
		} catch (Exception e) {
			//如果输入了错误的格式，则把correctOperation的值改为false，且弹出对话框
			correctOperation = false;
			JOptionPane.showMessageDialog(null, "请输入正确的手机号码格式", "警告",JOptionPane.WARNING_MESSAGE);
		}
		if(correctOperation) {
			newAccount.setId(account.getId());
			newAccount.setGender(genderTxt.getText());
			newAccount.setName(nameTxt.getText());
			newAccount.seteMail(emailTxt.getText());
			newAccount.setPhone(Long.parseLong(phoneTxt.getText()));
			boolean result = dao.changedInformation(newAccount);
			if(result) {
				JOptionPane.showMessageDialog(null, "修改成功！");
				this.dispose();
			}else {
				JOptionPane.showMessageDialog(null, "修改失败！", "警告", JOptionPane.WARNING_MESSAGE);
			}
		}
		
	}
	/**
	 * 填充文本框
	 * @param account 对于的用户信息的实体类
	 */
	private void fillText(Account account) {
		nameTxt.setText(account.getName());
		genderTxt.setText(account.getGender());
		emailTxt.setText(account.geteMail());
		phoneTxt.setText(String.valueOf(account.getPhone()));
	}
	
}
