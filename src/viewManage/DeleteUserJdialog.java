package viewManage;

import java.awt.BorderLayout;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.UserDao;
import model.User;
import util.DBUtil;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class DeleteUserJdialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField idTex;
	private JComboBox roleComboBox = new JComboBox();
	public User user = new User();
	public UserDao dao = new UserDao();

	public DeleteUserJdialog() {
		setTitle("\u6CE8\u9500\u7528\u6237\r\n");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u8D26\u53F7:");
		lblNewLabel.setFont(new Font("华文中宋", Font.PLAIN, 17));
		lblNewLabel.setBounds(89, 53, 54, 15);
		contentPanel.add(lblNewLabel);
		
		idTex = new JTextField();
		idTex.setBounds(148, 52, 156, 21);
		contentPanel.add(idTex);
		idTex.setColumns(10);
		
		roleComboBox.setModel(new DefaultComboBoxModel(new String[] {"\u7528\u6237", "\u56FE\u4E66\u7BA1\u7406\u5458"}));
		roleComboBox.setBounds(148, 121, 102, 23);
		contentPanel.add(roleComboBox);
		
		JLabel lblNewLabel_1 = new JLabel("\u8EAB\u4EFD\uFF1A\r\n");
		lblNewLabel_1.setFont(new Font("华文中宋", Font.PLAIN, 17));
		lblNewLabel_1.setBounds(89, 121, 54, 19);
		contentPanel.add(lblNewLabel_1);
		
		JButton affirmButton = new JButton("\u786E\u8BA4\r\n");
		affirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deletePerformed();
				
			}
		});
		affirmButton.setIcon(new ImageIcon(DeleteUserJdialog.class.getResource("/picture/\u6B63\u786E.png")));
		affirmButton.setFont(new Font("华文仿宋", Font.PLAIN, 15));
		affirmButton.setBounds(174, 189, 93, 23);
		contentPanel.add(affirmButton);
	}
/**
 * "删除"事件的处理
 */
	protected void deletePerformed() {
		//把界面信息封装到user类中
		user.setId(idTex.getText());
		user.setRole(roleComboBox.getSelectedItem().toString());
		Connection con = null;
		if(user.getId()!="") {
			try {
				con = DBUtil.getConnection();
				boolean result = dao.deleteUser(con, user);
				DBUtil.closeConnection(con);
				if(result) {
					this.dispose();									//若操作成功，则销毁本窗口
					JOptionPane.showMessageDialog(null, "删除成功");
				}
				else {
					JOptionPane.showMessageDialog(null, "删除失败");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			JOptionPane.showMessageDialog(null, "输入信息不能为空");
		}
	}
}
