package viewManage;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dao.UserDao;
import model.User;
import util.DBUtil;

public class ChangeUserJdialog extends JDialog {

	private final JPanel newPassword_1Tex = new JPanel();
	private JTextField idTex;
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;
	private JButton affirmButton ;
	
	private UserDao dao = new UserDao();
	private User user = new User();
	
	public ChangeUserJdialog(String id) {
		setBounds(100, 100, 474, 391);
		getContentPane().setLayout(new BorderLayout());
		newPassword_1Tex.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(newPassword_1Tex, BorderLayout.CENTER);
		newPassword_1Tex.setLayout(null);
		{
			JLabel idLabel = new JLabel("\u8D26\u53F7\uFF1A");
			idLabel.setFont(new Font("华文中宋", Font.PLAIN, 17));
			idLabel.setBounds(110, 89, 54, 21);
			newPassword_1Tex.add(idLabel);
		}
		
		idTex = new JTextField();
		idTex.setBounds(190, 89, 150, 21);
		newPassword_1Tex.add(idTex);
		idTex.setColumns(10);
		
		affirmButton = new JButton("\u786E\u8BA4");
		affirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeActionPerform();
			}
		});
		affirmButton.setIcon(new ImageIcon(ChangeUserJdialog.class.getResource("/picture/\u6B63\u786E.png")));
		affirmButton.setFont(new Font("华文仿宋", Font.PLAIN, 15));
		affirmButton.setBounds(189, 285, 93, 23);
		newPassword_1Tex.add(affirmButton);
		
		JLabel lblNewLabel_2 = new JLabel("\u65B0\u5BC6\u7801\uFF1A");
		lblNewLabel_2.setFont(new Font("华文中宋", Font.PLAIN, 17));
		lblNewLabel_2.setBounds(110, 145, 69, 15);
		newPassword_1Tex.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		lblNewLabel_3.setFont(new Font("华文中宋", Font.PLAIN, 17));
		lblNewLabel_3.setBounds(110, 195, 91, 23);
		newPassword_1Tex.add(lblNewLabel_3);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(190, 145, 150, 21);
		newPassword_1Tex.add(passwordField_1);
		
		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(190, 195, 150, 21);
		newPassword_1Tex.add(passwordField_2);
		setIdTxt(id);
	}
	private void setIdTxt(String id) {
		idTex.setText(id);
	}
	/**
	 * “修改"事件的处理
	 */
	protected void changeActionPerform() {
		String newPassword_1 = new String(passwordField_1.getPassword());
		String newPassword_2 = new String(passwordField_2.getPassword());
		Connection con = null;
		//如果前后输入的密码一致，则把界面中的信息封装到user类中
		if(newPassword_1.equals(newPassword_2)) {
			user.setId(idTex.getText());
			user.setPassword(newPassword_1);
			try {
				con = DBUtil.getConnection();
				boolean result = dao.changeUserInformation(con, user);
				DBUtil.closeConnection(con);
				//如果修改成功，则弹出对话框并销毁当前窗口
				if(result) {
					JOptionPane.showMessageDialog(null, "修改成功");
					this.dispose();
				}
				else {
					JOptionPane.showMessageDialog(null,"修改失败");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else {
			JOptionPane.showMessageDialog(null, "密码不一致!");
		}
	}
}
