package viewManage;


import java.util.HashMap;
import java.util.Vector;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;

import javax.swing.JTable;

import javax.swing.table.DefaultTableModel;

import dao.AccountDao;
import dao.UserDao;
import model.Account;
import model.User;


import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.awt.event.ActionEvent;

import java.awt.SystemColor;

public class ManageInterFra extends JInternalFrame {
	private JTextField searchIdTxt;
	private UserDao dao = new UserDao();
	private JTable table = new JTable();
	//用于储存从界面获取的账号
	private String id = new String();
	private AccountDao accountDao = new AccountDao();
	/**
	 * 该构造函数的参数用于传入获取的数据
	 */
	public ManageInterFra(Vector data,Vector column) {
		setBounds(200, 200, 690, 333);
		setClosable(true);
		setIconifiable(true);
		getContentPane().setLayout(null);
		
		searchIdTxt = new JTextField();
		searchIdTxt.setEnabled(true);
		searchIdTxt.setEditable(true);
		searchIdTxt.setText("");
		searchIdTxt.setBounds(201, 22, 125, 21);
		getContentPane().add(searchIdTxt);
		searchIdTxt.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("\u8D26\u53F7\uFF1A\r\n");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(139, 23, 65, 15);
		getContentPane().add(lblNewLabel);
		
		JButton searchButton = new JButton("\u67E5\u8BE2\r\n");
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchActionPerformed(searchIdTxt.getText(),column);
			}
		});
		searchButton.setIcon(new ImageIcon(ManageInterFra.class.getResource("/picture/\u67E5\u8BE2.png")));
		searchButton.setBounds(372, 15, 93, 32);
		getContentPane().add(searchButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(1, 116, 543, 188);
		getContentPane().add(scrollPane);
		//把表格添加到面板中
		scrollPane.setViewportView(table);
		//为修改事件添加监听器
		JButton changeButton = new JButton("\u4FEE\u6539");
		changeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeAction();
			}
		});
		changeButton.setBackground(SystemColor.controlLtHighlight);
		changeButton.setIcon(new ImageIcon(ManageInterFra.class.getResource("/picture/\u4FEE\u6539.png")));
		changeButton.setBounds(566, 192, 104, 30);
		getContentPane().add(changeButton);
		//为删除事件添加监听器
		JButton delectButton = new JButton("\u6CE8\u9500");
		delectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delectAction();
			}
		});
		delectButton.setBackground(SystemColor.controlLtHighlight);
		delectButton.setIcon(new ImageIcon(ManageInterFra.class.getResource("/picture/\u5220\u9664-1.png")));
		delectButton.setBounds(567, 243, 104, 30);
		getContentPane().add(delectButton);
		
		JButton getInformationButton = new JButton("\u8D26\u53F7\u4FE1\u606F");
		//为账号信息按钮添加监听器
		getInformationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getInformationAction();
			}
		});
		getInformationButton.setBackground(SystemColor.controlLtHighlight);
		getInformationButton.setIcon(new ImageIcon(ManageInterFra.class.getResource("/picture/\u501F\u9605\u7533\u8BF7.png")));
		getInformationButton.setBounds(564, 144, 104, 30);
		getContentPane().add(getInformationButton);
		//初始化表格
		fillTable(data,column);
		addListener();
	}
	
	private void getInformationAction() {
		User user = new User();
		user.setId(id);
		Account account = null;
		account = accountDao.getAccount(user);
		if(account.getId()!=null) {
			new InformationOfUser(account).setVisible(true);
		}else {
			JOptionPane.showMessageDialog(null, "该账号的信息已经在该界面呈现了", "错误提示", JOptionPane.ERROR_MESSAGE);
		}
	}
	/**
	 * 修改事件
	 */
	private void changeAction() {
		if(!id.equals("")) {
			new ChangeUserJdialog(id).setVisible(true);
		}else {
			JOptionPane.showMessageDialog(null, "请选中要操作的数据所在行", "警告", JOptionPane.WARNING_MESSAGE);
		}
	}
	/**
	 * 删除事件
	 */
	private void delectAction() {
		if(!id.equals("")) {
			boolean deleteInUser = dao.delect(id);
			boolean deleteInAccount = accountDao.delete(id);
			//重置id
			id="";
			if(deleteInUser&&deleteInAccount) {
				JOptionPane.showMessageDialog(null,"删除成功");
				//重置表格
				fillTable(dao.searchAllUser().get("data"), dao.searchAllUser().get("column"));
			}else {
				JOptionPane.showMessageDialog(null, "操作失败", "警告", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(null, "请选中要操作的数据所在行", "警告", JOptionPane.WARNING_MESSAGE);
		}
	}
/**
 * 为表格添加监听器
 */
	private void addListener() {
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO 自动生成的方法存根
				super.mouseClicked(e);
				//获取点击行的id值
				id = table.getValueAt(table.getSelectedRow(),0).toString();
			}
		});
	}

	private void fillTable(Vector data,Vector column) {
		DefaultTableModel model = (DefaultTableModel)table.getModel();
		model.setDataVector(data, column);
		table.setModel(model);
	}

	private void searchActionPerformed(String id,Vector column) {
		if(!id.equals("")) {
			Vector<Vector<String>> dataOfSearching = dao.searchUser(id);
			fillTable(dataOfSearching, column);
		}else {
			fillTable(dao.searchAllUser().get("data"), column);
		}
	}
}

