package viewManage;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;


import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.AccountDao;
import dao.UserDao;
import model.Account;
import model.User;
import util.DBUtil;
import javax.swing.ImageIcon;
import javax.swing.JPasswordField;

public class AddUserJdialog extends JDialog {
	private JTextField idTex;
	private JComboBox roleComboBox = new JComboBox();
	private JButton affirmButton = new JButton("\u786E\u8BA4");
	private UserDao dao = new UserDao();
	private AccountDao accountDao = new AccountDao();
	private JPasswordField passwordTex;
	/**
	 * Create the dialog.
	 */
	public AddUserJdialog() {
		setTitle("\u7528\u6237\u589E\u6DFB");
		setBounds(100, 100, 450, 333);
		getContentPane().setLayout(null);
		
		idTex = new JTextField();
		idTex.setBounds(159, 56, 172, 21);
		getContentPane().add(idTex);
		idTex.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("\u8D26\u53F7\uFF1A");
		lblNewLabel.setFont(new Font("华文中宋", Font.PLAIN, 15));
		lblNewLabel.setBounds(108, 57, 54, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u5BC6\u7801\uFF1A");
		lblNewLabel_1.setFont(new Font("华文中宋", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(108, 114, 54, 15);
		getContentPane().add(lblNewLabel_1);
		
		affirmButton.setFont(new Font("华文仿宋", Font.PLAIN, 15));
		affirmButton.setIcon(new ImageIcon(AddUserJdialog.class.getResource("/picture/\u6B63\u786E.png")));
		affirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				insertActionPerformed();
			}
		});
		affirmButton.setBounds(260, 228, 93, 23);
		getContentPane().add(affirmButton);
		
		
		roleComboBox.setFont(new Font("华文楷体", Font.PLAIN, 14));
		roleComboBox.setModel(new DefaultComboBoxModel(new String[] {"\u7528\u6237", "\u56FE\u4E66\u7BA1\u7406\u5458"}));
		roleComboBox.setBounds(159, 166, 107, 23);
		getContentPane().add(roleComboBox);
		
		JLabel lblNewLabel_2 = new JLabel("\u8EAB\u4EFD\uFF1A");
		lblNewLabel_2.setFont(new Font("华文中宋", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(108, 166, 54, 21);
		getContentPane().add(lblNewLabel_2);
		
		passwordTex = new JPasswordField();
		passwordTex.setBounds(159, 113, 172, 21);
		getContentPane().add(passwordTex);
	}
	/**
	 * 插入数据事件
	 */
	protected void insertActionPerformed() {
		//将输入框中的信息封装到User类中
		User user = new User(idTex.getText(), new String(passwordTex.getPassword()),roleComboBox.getSelectedItem().toString() );
		Connection con = null;
		if(!user.isEmptyOrNot()) {
			try {
				con = DBUtil.getConnection();
				boolean insertedIntoUser = dao.insertInformation(con,user);
				boolean insertedIntoAccount = accountDao.addAccount(con, new Account(idTex.getText()));
				DBUtil.closeConnection(con);
				if(insertedIntoUser&&insertedIntoAccount) {
					this.dispose();
					JOptionPane.showMessageDialog(null, "增添成功");
				}
				else {
					JOptionPane.showMessageDialog(null, "增添失败");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			}
		else {
			JOptionPane.showMessageDialog(null, "输入信息不能为空");
		}
		}
	}
