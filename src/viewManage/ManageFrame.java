package viewManage;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import dao.UserDao;

import util.OutLog;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


import java.awt.Color;
import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;


public class ManageFrame extends JFrame {

	private JPanel contentPane;
	private UserDao userDao = new UserDao();
	private OutLog outLog = new OutLog();
	/**
	 * Create the frame.
	 */
	public ManageFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 608, 447);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("\u57FA\u672C\u6570\u636E\u7EF4\u62A4");
		mnNewMenu.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u7EF4\u62A4.png")));
		mnNewMenu.setHorizontalAlignment(SwingConstants.LEFT);
		menuBar.add(mnNewMenu);
		
		JMenuItem searchMenultem = new JMenuItem("\u67E5\u8BE2\u7528\u6237");
		searchMenultem.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u67E5\u8BE2.png")));
		//为查询事件添加监视器
		searchMenultem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManageInterFra manageInterFra = new ManageInterFra(userDao.searchAllUser().get("data"), userDao.searchAllUser().get("column"));
				manageInterFra.setVisible(true);
				contentPane.add(manageInterFra);
			}
		});
		mnNewMenu.add(searchMenultem);
		//为修改事件添加监视器
		JMenuItem changeMenuItem = new JMenuItem("\u4FEE\u6539\u7528\u6237\u4FE1\u606F");
		changeMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ChangeUserJdialog("").setVisible(true);
			}
		});
		changeMenuItem.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u4FEE\u6539.png")));
		mnNewMenu.add(changeMenuItem);
		//为添加事件添加监视器
		JMenuItem addMenultem = new JMenuItem("\u589E\u6DFB\u7528\u6237");
		addMenultem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AddUserJdialog().setVisible(true);
			}
		});
		addMenultem.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u589E\u52A0.png")));
		mnNewMenu.add(addMenultem);
		//为删除事件添加监视器
		JMenuItem deleteMenuItem = new JMenuItem("\u6CE8\u9500\u7528\u6237");
		deleteMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DeleteUserJdialog().setVisible(true);
			}
		});
		deleteMenuItem.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u6CE8\u9500.png")));
		mnNewMenu.add(deleteMenuItem);
		
		JMenu mnNewMenu_1 = new JMenu("\u5176\u4ED6");
		mnNewMenu_1.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u5176\u4ED6.png")));
		menuBar.add(mnNewMenu_1);
		//为退出事件添加监视器
		JMenuItem LogOutMenuItem = new JMenuItem("\u9000\u51FA\u8D26\u53F7");
		LogOutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outLog.outLogActionPerform(ManageFrame.this);
			}
		});
		LogOutMenuItem.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u9000\u51FA.png")));
		mnNewMenu_1.add(LogOutMenuItem);

		
		
		JLabel lblNewLabel =new JLabel("\u6B22\u8FCE\u60A8\uFF0C\u7CFB\u7EDF\u7BA1\u7406\u5458\uFF01");
		lblNewLabel.setFont(new Font("华文行楷", Font.PLAIN, 25));
		lblNewLabel.setIcon(new ImageIcon(ManageFrame.class.getResource("/picture/\u7BA1\u7406.png")));
		lblNewLabel.setBounds(432, 10, 387, 199);
		contentPane.add(lblNewLabel);
	}

	
}
