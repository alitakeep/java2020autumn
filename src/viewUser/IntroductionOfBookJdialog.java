package viewUser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;


import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Toolkit;

public class IntroductionOfBookJdialog extends JDialog {

	JLabel photoJLable;

	/**
	 * Create the dialog.
	 */
	public IntroductionOfBookJdialog(ImageIcon icon,String text) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(IntroductionOfBookJdialog.class.getResource("/picture/\u4E66\u7C4D\u8BE6\u60C5.png")));
		setTitle("\u4E66\u7C4D\u8BE6\u60C5");
		setBounds(100, 100, 658, 500);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "内容简介", TitledBorder.CENTER,TitledBorder.TOP, new Font("华文楷体", Font.PLAIN, 16),Color.pink));
		panel.setBounds(301, 0, 350, 463);
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setForeground(new Color(0, 0, 0));
		textArea.setLineWrap(true);
		textArea.setFont(new Font("楷体", Font.PLAIN, 15));
		textArea.setText(text);
		scrollPane.setViewportView(textArea);
		
		JPanel photoPanel = new JPanel();
		photoPanel.setBounds(3, -1, 300, 462);
		getContentPane().add(photoPanel);
		photoPanel.setLayout(new BorderLayout(0, 0));
		
		photoJLable = new JLabel("");
		photoJLable.setBounds(0, 0, 350, 460);
		//如果没有添加图片，则不执行改变图片大小的操作
		if(icon!=null) {
		icon.getImage().getScaledInstance(300, 260, Image.SCALE_DEFAULT);
		photoJLable.setIcon(icon);
		
		//photoJLable.setBounds(0, 0, icon.getIconWidth(),icon.getIconHeight());
		}else {
			photoJLable.setText("暂无图片");
		}
		photoPanel.add(photoJLable,BorderLayout.CENTER);
	}
	
}

