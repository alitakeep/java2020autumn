package viewUser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import dao.AccountDao;
import dao.RenewBookDao;
import model.Account;
import model.RenewBook;
import util.PackagingTabletextInTextField;
import util.RoundButton;

import javax.swing.JScrollPane;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BookRenewalJdialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private JLabel bookNameLable;
	private JLabel idOfBookLable;
	private JLabel authorNameLable;
	private JLabel DueDateLable;
	private JLabel renewalTimesLable;
	
	private AccountDao dao = new AccountDao();
	private RenewBook renewBook = new RenewBook();
	private RenewBookDao renewBookDao = new RenewBookDao();
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	
	
	/**
	 * Create the dialog.
	 */
	public BookRenewalJdialog(Account account) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(BookRenewalJdialog.class.getResource("/picture/\u8FDC\u7A0B\u501F\u9605\u7EDF\u8BA1.png")));
		setBounds(100, 100, 663, 444);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 649, 158);
		contentPanel.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "借阅情况", TitledBorder.LEADING,TitledBorder.TOP, new Font("华文楷体", Font.PLAIN, 16),Color.pink));
		panel.setBounds(0, 213, 493, 184);
		contentPanel.add(panel);
		panel.setLayout(null);
		
		bookNameLable = new JLabel("");
		bookNameLable.setFont(new Font("华文楷体", Font.PLAIN, 14));
		bookNameLable.setBounds(52, 37, 307, 24);
		panel.add(bookNameLable);
		
		idOfBookLable = new JLabel("");
		idOfBookLable.setFont(new Font("华文楷体", Font.PLAIN, 14));
		idOfBookLable.setBounds(75, 86, 114, 24);
		panel.add(idOfBookLable);
		
		authorNameLable = new JLabel("");
		authorNameLable.setFont(new Font("华文楷体", Font.PLAIN, 14));
		authorNameLable.setBounds(47, 135, 212, 24);
		panel.add(authorNameLable);
		
		DueDateLable = new JLabel("");
		DueDateLable.setFont(new Font("华文楷体", Font.PLAIN, 14));
		DueDateLable.setBounds(307, 83, 137, 24);
		panel.add(DueDateLable);
		
		renewalTimesLable = new JLabel("");
		renewalTimesLable.setFont(new Font("华文楷体", Font.PLAIN, 14));
		renewalTimesLable.setBounds(308, 139, 52, 24);
		panel.add(renewalTimesLable);
		
		lblNewLabel = new JLabel("\u4E66\u540D\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 14));
		lblNewLabel.setBounds(11, 41, 54, 15);
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("\u56FE\u4E66\u7F16\u53F7\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(8, 92, 72, 15);
		panel.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("\u4F5C\u8005\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(9, 140, 54, 15);
		panel.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("\u622A\u6B62\u65E5\u671F\uFF1A");
		lblNewLabel_3.setFont(new Font("华文楷体", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(240, 84, 76, 23);
		panel.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("\u7EED\u501F\u6B21\u6570\uFF1A");
		lblNewLabel_4.setFont(new Font("华文楷体", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(242, 140, 74, 21);
		panel.add(lblNewLabel_4);
		
		JButton renewBookButton = new RoundButton("续借", new Color(254, 67, 101), new Color(252, 157, 154));
		//为续借按钮添加监听器
		renewBookButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				renewAction(account);
			}
		});
		renewBookButton.setForeground(UIManager.getColor("Button.disabledShadow"));
		renewBookButton.setBackground(Color.WHITE);
		renewBookButton.setIcon(new ImageIcon(BookRenewalJdialog.class.getResource("/picture/\u501F\u9605\u7533\u8BF7.png")));
		renewBookButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		renewBookButton.setBounds(531, 274, 108, 29);
		contentPanel.add(renewBookButton);
		
		table = new JTable();
		setDefaultTable(table,scrollPane);
		scrollPane.setViewportView(table);
		fillTable(account);
		/**
		 * 由于视图无法直接更新，所以当该窗体关闭时，把对应的视图删除。这样下一次查询时才能重新刷新视图，避免呈现错误数据。
		 */
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO 自动生成的方法存根
				super.windowClosing(e);
				dao.dropTheView(account.getId());
			}
		});
		fillJLabel();
	}
	private void renewAction(Account accout) {
		int numberOfRenew = 0;
		try {
			numberOfRenew = Integer.parseInt(renewalTimesLable.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "还没选中书籍呢", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/picture/难过.png")));
		}
		
		//如果续借次数已达到两次，则弹出提示框
		if(numberOfRenew==2) {
			JOptionPane.showMessageDialog(null, "已经不能再续借了", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/picture/难过.png")));
		}else if(!idOfBookLable.getText().equals("")) {
			renewBook.setId(accout.getId());
			renewBook.setIdOfBook(idOfBookLable.getText());
			renewBook.setBookName(bookNameLable.getText());
			renewBook.setNumberOfRenew(Integer.parseInt(renewalTimesLable.getText()));
			int result = renewBookDao.setMessage(renewBook);
			if(result==1) {
				JOptionPane.showMessageDialog(null, "等待管理员的审核", "", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/picture/敬请期待.png")));
			}else if(result==0) {
				JOptionPane.showMessageDialog(null, "续借失败了", "警告", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/picture/难过.png")));
			}else {
				JOptionPane.showMessageDialog(null, "这本书已经申请过了，请给管理员一点时间吧", "温馨提示", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/picture/难过.png")));
			}
		}else {
			JOptionPane.showMessageDialog(null, "请选择你要续借的书呀", "温馨提示", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/picture/难过.png")));
		}
	}
	/**
	 * 把信息填充至标签中。
	 */
	private void fillJLabel() {
		Vector<JLabel> vectorOfJLabels = packageJLabels();
		table.addMouseListener(new PackagingTabletextInTextField(vectorOfJLabels, table));
	}
	
	/**
	 * 将相应的数据填入表格中
	 * @param account
	 */
	private void fillTable(Account account) {
		dao.createView(account.getId());
		Map<String, Vector> mapOfVector = new HashMap<String, Vector>();
		mapOfVector = dao.getLengingInformationForUser(account.getId());
		DefaultTableModel model = (DefaultTableModel)table.getModel();
		model.setDataVector(mapOfVector.get("data"), mapOfVector.get("column"));
		table.setModel(model);
	}

	/**
	 * 初始化表格属性，并将其添加到JScrollPane中
	 * @param table
	 * @param scrollPane
	 */
	private void setDefaultTable(JTable table,JScrollPane scrollPane) {
		table.setEnabled(true);
		table.setRowSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(false);
		
	}
	//把JLable装进向量中
	private Vector<JLabel> packageJLabels() {
		Vector<JLabel> vector = new Vector<JLabel>();
		vector.add(idOfBookLable);
		vector.add(bookNameLable);
		vector.add(authorNameLable);
		vector.add(DueDateLable);
		vector.add(renewalTimesLable);
		return vector;
	}
}
