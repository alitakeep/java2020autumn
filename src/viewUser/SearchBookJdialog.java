package viewUser;

import java.awt.BorderLayout;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dao.BookDao;

import java.awt.Toolkit;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import util.IsTextEmptyOrNot;
import util.RoundButton;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class SearchBookJdialog extends JDialog {
	private JTable table;
	private JPanel contentPanel = new JPanel();
	private final JScrollPane scrollPane = new JScrollPane();
	private final JTextField bookNameTxt = new JTextField();
	private final JTextField authorNameTxt = new JTextField();
	
	
	private BookDao bookDao = new BookDao();
	private IsTextEmptyOrNot isTextEmptyOrNot = new IsTextEmptyOrNot();
	//用于记录书籍编号
	private String idOfBook;
	
	/**
	 * Create the dialog.
	 */
	public SearchBookJdialog() {
		//添加表格
		table = new JTable();
		scrollPane.setViewportView(table);
		//初始化表格
		fillTable();
		//给表格添加监听器，以获取点击内容
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				//记录下所选行的图书编号
				idOfBook = table.getValueAt(table.getSelectedRow(), 0).toString();
			}
		});
		
		authorNameTxt.setBounds(136, 70, 156, 21);
		authorNameTxt.setColumns(10);
		
		bookNameTxt.setBounds(136, 19, 156, 21);
		bookNameTxt.setColumns(10);
		
		setTitle("\u67E5\u627E\u4F60\u60F3\u8981\u7684\u4E66\u7C4D\u5427\uFF01");
		setIconImage(Toolkit.getDefaultToolkit().getImage(SearchBookJdialog.class.getResource("/picture/\u67E5\u8BE2\u56FE\u4E66.png")));
		setBounds(100, 100, 565, 457);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		scrollPane.setBounds(1, 126, 549, 228);
		
		contentPanel.add(scrollPane);
		
		contentPanel.add(bookNameTxt);
		
		contentPanel.add(authorNameTxt);
		
		JLabel lblNewLabel = new JLabel("\u4E66\u7C4D\u540D\u79F0\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(50, 16, 83, 24);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u4F5C\u8005\u540D\u79F0\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(54, 68, 88, 24);
		contentPanel.add(lblNewLabel_1);
		
		JButton searchButton = new RoundButton("查找", new Color(255, 245, 247),new Color(192,247,252));
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchActionPerformed(table);
			}
		});
		searchButton.setBackground(SystemColor.control);
		searchButton.setForeground(Color.BLACK);
		searchButton.setIcon(new ImageIcon(SearchBookJdialog.class.getResource("/picture/\u56FE\u4E66\u67E5\u627E.png")));
		searchButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		searchButton.setBounds(404, 43, 93, 29);
		contentPanel.add(searchButton);
		
		JLabel lblNewLabel_2 = new JLabel("\u70B9\u51FB\u8BE6\u60C5\u53EF\u67E5\u770B\u5185\u5BB9\u7B80\u4ECB\uFF01");
		lblNewLabel_2.setIcon(new ImageIcon(SearchBookJdialog.class.getResource("/picture/\u5F00\u5FC3.png")));
		lblNewLabel_2.setForeground(new Color(0, 0, 0));
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(11, 364, 232, 46);
		contentPanel.add(lblNewLabel_2);
		
		JButton detailsButton = new RoundButton("详情", 0);
		detailsButton.setBackground(SystemColor.controlLtHighlight);
		//为详情按钮添加监视器
		detailsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showDetails();
			}
		});
		detailsButton.setIcon(new ImageIcon(SearchBookJdialog.class.getResource("/picture/\u8BE6\u60C5\u67E5\u770B.png")));
		detailsButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		detailsButton.setBounds(253, 376, 93, 30);
		contentPanel.add(detailsButton);
		
		
	}

	/**
	 * 创建一个窗口放置对应图书的封面图片和内容简介
	 * @param idOfBook 用户点击某一行时该行中存放的图书编号
	 */
	protected void showDetails() {
		if(idOfBook!=null) {
			//获取对应图书的封面图片
			ImageIcon icon = bookDao.getImageIcon(idOfBook);
			//获取对应图书的内容简介
			String text = bookDao.getArticle(idOfBook);
			new IntroductionOfBookJdialog(icon,text).setVisible(true);
		}else {
			JOptionPane.showMessageDialog(null, "您还没有选中心仪的图书","温馨提示", JOptionPane.WARNING_MESSAGE, new ImageIcon(getClass().getResource("/picture/难过.png")));
		}
	}
/**
 * 该方法用于处理用户查询命令
 * 由于查询界面有两个输入框。 即用户有三种方式进行查询，一种是只通过书籍名称进行查询；一种是只通过作者名称进行查询；最后一种是通过书名和作者进行精确查询
 * 所以第一步即是通过获取两个文本框的内容来判断用户是要进行哪种查询操作。
 * 再得知用户的操作类别后，把对应的条件语句附加到主干查询语句之后。再把整个sql语句作为参数传入dao包中进行相应的操作
 * @param table 该类的table组件
 */
	protected void searchActionPerformed(JTable table) {
		boolean haveBookName = isTextEmptyOrNot.isTheTextEmptyOrNot(bookNameTxt);
		boolean haveAuthorName = isTextEmptyOrNot.isTheTextEmptyOrNot(authorNameTxt);
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		StringBuffer sql = new StringBuffer("select BOOKNUMBER,BOOKNAME,BOOKTYPE,BOOKCASE,AUTHOR from book_information ");
		if(haveBookName==true && haveAuthorName==false) {
			sql.append("where bookName like '%"+bookNameTxt.getText()+"%'");
		}else if (haveBookName==false && haveAuthorName==true) {
			sql.append("where author like '%"+authorNameTxt.getText()+"%'");
		}else if (haveBookName && haveAuthorName) {
			//如果两个文本框都不为空
			sql.append("where bookName like '%"+bookNameTxt.getText()+"%' and  author like '%"+authorNameTxt.getText()+"%'");
		}else {
			
		}
		data = bookDao.search(sql.toString());
		//把获取的信息呈现在表格中
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setDataVector(data, bookDao.searchAllBook().get(2));
	}

	/**
	 * 初始化表格
	 * @param table 该面板中的表格组件
	 */
	private void fillTable() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setDataVector(bookDao.searchAllBook().get(1), bookDao.searchAllBook().get(2));
		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	}
}
