package viewUser;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.AccountDao;
import model.Account;
import model.User;
import util.ChangeBackground;
import util.GetTime;
import util.RoundButton;


public class UserJFrame extends JFrame {

	private JPanel contentPane;
	private JLabel idLable;
	private	JLabel nameLabel;
	private JLabel genderLable;
	private JLabel phoneLable;
	private JLabel eMailLable;
	private AccountDao dao = new AccountDao();
	private JLabel timeLable = new JLabel("");
	private Account account;
	private GetTime setJFrame;
	/**
	 * Create the frame.
	 */
	public UserJFrame(User user) {
		
		account = dao.getAccount(user);
		setDefaultJFrame(account.getName());
		setFont(new Font("华文行楷", Font.PLAIN, 12));
		setIconImage(Toolkit.getDefaultToolkit().getImage(UserJFrame.class.getResource("/picture/\u7528\u6237.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 689, 454);
		//设置背景
		ImageIcon icon = new ImageIcon(this.getClass().getResource("/picture/sea.jpg"));
		contentPane = new ChangeBackground(icon);
		contentPane.setBackground(new Color(255, 250, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel JLable = new JLabel("");
		JLable.setFont(new Font("华文行楷", Font.PLAIN, 20));
		JLable.setIcon(new ImageIcon(UserJFrame.class.getResource("/picture/\u56FE\u4E66\u9986\u7535\u8111.png")));
		JLable.setBounds(33, 28, 143, 139);
		contentPane.add(JLable);
		
		idLable = new JLabel("$$$$$");
		idLable.setForeground(Color.WHITE);
		idLable.setFont(new Font("华文行楷", Font.PLAIN, 25));
		idLable.setBounds(54, 200, 231, 30);
		contentPane.add(idLable);
		
		nameLabel = new JLabel("New label");
		nameLabel.setForeground(Color.WHITE);
		nameLabel.setFont(new Font("华文行楷", Font.PLAIN, 25));
		nameLabel.setBounds(50, 260, 297, 30);
		contentPane.add(nameLabel);
		
		genderLable = new JLabel("New label");
		genderLable.setForeground(Color.WHITE);
		genderLable.setFont(new Font("华文行楷", Font.PLAIN, 25));
		genderLable.setBounds(303, 198, 116, 30);
		contentPane.add(genderLable);
		//为修改个人信息按钮添加监听器
		JButton changedInformationButton = new RoundButton("修改个人信息", 1);
		changedInformationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ChangedPersonInformationJdialog(account,UserJFrame.this).setVisible(true);
			}
		});
		changedInformationButton.setBackground(new Color(245, 222, 179));
		changedInformationButton.setIcon(new ImageIcon(UserJFrame.class.getResource("/picture/\u94A2\u7B14.png")));
		changedInformationButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		changedInformationButton.setBounds(481, 104, 160, 30);
		contentPane.add(changedInformationButton);
		
		JButton searchBookButton = new RoundButton("图书查询",1);
		//为查询按钮添加监听器
		searchBookButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SearchBookJdialog().setVisible(true);
			}
		});
		searchBookButton.setBackground(new Color(245, 222, 179));
		searchBookButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		searchBookButton.setIcon(new ImageIcon(UserJFrame.class.getResource("/picture/\u7528\u6237-\u67E5\u8BE2.png")));
		searchBookButton.setBounds(504, 218, 125, 30);
		contentPane.add(searchBookButton);
		
		phoneLable = new JLabel("");
		phoneLable.setForeground(Color.WHITE);
		phoneLable.setFont(new Font("华文行楷", Font.PLAIN, 25));
		phoneLable.setBounds(50, 310, 297, 30);
		contentPane.add(phoneLable);
		
		eMailLable = new JLabel("");
		eMailLable.setForeground(Color.WHITE);
		eMailLable.setFont(new Font("华文行楷", Font.PLAIN, 25));
		eMailLable.setBounds(52, 364, 340, 31);
		contentPane.add(eMailLable);
		
		JButton viewMyBorrowingButton = new RoundButton("我的借阅", 1);
		//为查看借阅按钮添加监听器
		viewMyBorrowingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BookRenewalJdialog(account).setVisible(true);
			}
		});
		viewMyBorrowingButton.setBackground(new Color(245, 222, 179));
		viewMyBorrowingButton.setIcon(new ImageIcon(UserJFrame.class.getResource("/picture/\u501F\u9605\u8BB0\u5F55.png")));
		viewMyBorrowingButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		viewMyBorrowingButton.setBounds(504, 285, 125, 30);
		contentPane.add(viewMyBorrowingButton);
		
		JButton readMessageButton = new RoundButton("我的信息", 1);
		//为查询信息按钮添加监听器
		readMessageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ReadMessageJdialog(user).setVisible(true);
			}
		});
		readMessageButton.setIcon(new ImageIcon(UserJFrame.class.getResource("/picture/\u5DF2\u8BFB\u4FE1\u606F.png")));
		readMessageButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		readMessageButton.setBackground(new Color(245, 222, 179));
		readMessageButton.setBounds(504, 351, 125, 30);
		contentPane.add(readMessageButton);
		timeLable.setForeground(Color.BLACK);
		
		timeLable.setFont(new Font("华文行楷", Font.PLAIN, 25));
		timeLable.setBounds(194, 39, 334, 55);
		contentPane.add(timeLable);
		
		JButton changePasswordButton = new RoundButton("修改密码", 1);
		//为修改密码按钮添加监听器
		changePasswordButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ChangePasswordJdialog(account, UserJFrame.this).setVisible(true);
			}
		});
		
		changePasswordButton.setIcon(new ImageIcon(UserJFrame.class.getResource("/picture/\u7528\u6237\u4FEE\u6539\u5BC6\u7801.png")));
		changePasswordButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		changePasswordButton.setBounds(504, 163, 125, 30);
		contentPane.add(changePasswordButton);
		//填充JLable
		fillLable(account);
	
	}
	
	//更新窗口标题中的用户昵称
	public void changeName(String newName) {
		setJFrame.changeName(newName);
	}
	/**
	 * 该方法为窗口设置标语
	 * @param lable 
	 * @param lable 
	 */
	protected  void setDefaultJFrame(String name) {
		setJFrame = new GetTime(this, name,timeLable);
		setJFrame.start();
	}
	/**
	 * 该方法把对应的账号信息填充到主页面板中
	 * @param account
	 */
	private void fillLable(Account account) {
		idLable.setText("账号："+account.getId());
		nameLabel.setText("昵称："+account.getName());
		genderLable.setText("性别："+account.getGender());
		phoneLable.setText("电话号码："+String.valueOf(account.getPhone()));
		eMailLable.setText("邮箱："+account.geteMail());
	}
	public void refreshLable(Account newAccount) {
		nameLabel.setText("昵称："+newAccount.getName());
		phoneLable.setText("电话号码："+String.valueOf(newAccount.getPhone()));
		eMailLable.setText("邮箱："+newAccount.geteMail());
	}
}
