package viewUser;

import java.awt.BorderLayout;


import javax.swing.JButton;
import javax.swing.JDialog;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.AccountDao;
import model.Account;
import util.IsTextEmptyOrNot;
import util.RoundButton;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;

public class ChangedPersonInformationJdialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nameTxt;
	private JTextField phoneTxt;
	private JTextField eMailTxt;
	
	private Account newAccount;
	private UserJFrame userFrame;
	private AccountDao dao = new AccountDao();
	private IsTextEmptyOrNot isTextEmptyOrNot = new IsTextEmptyOrNot();
	

	/**
	 * Create the dialog.
	 */
	public ChangedPersonInformationJdialog(Account account,UserJFrame userFrame) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChangedPersonInformationJdialog.class.getResource("/picture/\u7528\u6237\u4FEE\u6539.png")));
		newAccount = account;
		this.userFrame = userFrame;
		setBounds(100, 100, 386, 340);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(255, 250, 240));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		nameTxt = new JTextField();
		nameTxt.setBounds(145, 43, 125, 21);
		contentPanel.add(nameTxt);
		nameTxt.setColumns(10);
		
		phoneTxt = new JTextField();
		phoneTxt.setColumns(10);
		phoneTxt.setBounds(145, 101, 125, 21);
		contentPanel.add(phoneTxt);
		
		eMailTxt = new JTextField();
		eMailTxt.setColumns(10);
		eMailTxt.setBounds(145, 162, 125, 21);
		contentPanel.add(eMailTxt);
		
		JLabel lblNewLabel = new JLabel("\u6635\u79F0\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(79, 44, 54, 15);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u7535\u8BDD\u53F7\u7801\uFF1A\r\n");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(51, 102, 86, 15);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u7535\u5B50\u90AE\u7BB1\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(60, 163, 86, 15);
		contentPanel.add(lblNewLabel_2);
		
		JButton changedButton = new RoundButton("修改", 0);
		changedButton.setBackground(SystemColor.control);
//		为修改按钮添加监听器
		changedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changedActionPerformed(ChangedPersonInformationJdialog.this.userFrame,newAccount);
			}
		});
		changedButton.setIcon(new ImageIcon(ChangedPersonInformationJdialog.class.getResource("/picture/\u94A2\u7B14.png")));
		changedButton.setFont(new Font("楷体", Font.PLAIN, 16));
		changedButton.setBounds(138, 234, 93, 27);
		contentPanel.add(changedButton);
		//将该账号信息填充到对应的文本框中
		fillTxt(account);
		this.getRootPane().setDefaultButton(changedButton);
	}
	/**
	 * 将个人信息填充到相应的文本框中
	 * @param account
	 */
	private void fillTxt(Account account) {
		nameTxt.setText(account.getName());
		phoneTxt.setText(String.valueOf(account.getPhone()));
		eMailTxt.setText(account.geteMail());
	}
	/**
	 * 处理修改事件
	 * 该方法除了修改数据库对应数据外还必需负责更新主面板的信息
	 * @param userFrame	用户登录后的主面板
	 * @param newAccount 封装了账号信息的类，与用户登录后的主面板共用同一个类，以便于实时更新。
	 * 
	 */
	private void changedActionPerformed(UserJFrame userFrame,Account newAccount) {
		//判断输入的信息是否为空
		if(isTextEmptyOrNot.isTheTextEmptyOrNot(nameTxt,"昵称")&&isTextEmptyOrNot.isTheTextEmptyOrNot(eMailTxt,"邮箱")&&
				isTextEmptyOrNot.isTheTextEmptyOrNot(phoneTxt,"电话号码")) {
			Icon iconOfTrue = new ImageIcon(getClass().getResource("/picture/笑脸.png"));
			Icon iconOfFalse = new ImageIcon(getClass().getResource("/picture/难过.png"));
			newAccount.setName(nameTxt.getText());
			newAccount.setPhone(Long.valueOf(phoneTxt.getText()));
			newAccount.seteMail(eMailTxt.getText());
			boolean result = dao.changedInformation(newAccount);
			if (result) {
				JOptionPane.showMessageDialog(null, "修改成功！", "提示信息", JOptionPane.INFORMATION_MESSAGE, iconOfTrue);
				//同时更新主面板的个人信息
				userFrame.refreshLable(newAccount);
				//同时更新窗口标题提示语
				userFrame.changeName(newAccount.getName());
				this.dispose();
			}else {
				JOptionPane.showMessageDialog(null, "修改失败", "警告", JOptionPane.INFORMATION_MESSAGE, iconOfFalse);
			}
		}
	}
}
