package viewUser;



import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import dao.UserDao;
import model.Account;
import model.User;
import util.RoundButton;
import view.BookManageSystem;
import view.LoginJFrame;

public class ChangePasswordJdialog extends JDialog {
	private JPasswordField newPasswordTxt;
	private JPasswordField confirmPasswordTxt;
	private UserDao dao = new UserDao();
	/**
	 * Create the dialog.
	 */
	public ChangePasswordJdialog(Account account,JFrame userJFrame) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChangePasswordJdialog.class.getResource("/picture/\u4FEE\u6539\u5BC6\u7801.png")));
		getContentPane().setBackground(SystemColor.info);
		setBounds(100, 100, 378, 300);
		getContentPane().setLayout(null);
		
		newPasswordTxt = new JPasswordField();
		newPasswordTxt.setBounds(153, 51, 136, 21);
		getContentPane().add(newPasswordTxt);
		
		confirmPasswordTxt = new JPasswordField();
		confirmPasswordTxt.setBounds(153, 105, 136, 21);
		getContentPane().add(confirmPasswordTxt);
		
		JLabel lblNewLabel_1 = new JLabel("\u65B0\u5BC6\u7801\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(86, 52, 82, 15);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u786E\u8BA4\u60A8\u7684\u5BC6\u7801\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(35, 106, 133, 15);
		getContentPane().add(lblNewLabel_2);
		
		JButton confirmButton = new RoundButton("确认",0);
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changedPasswordAction(account,userJFrame);
			}
		});
		confirmButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		confirmButton.setIcon(new ImageIcon(ChangePasswordJdialog.class.getResource("/picture/\u540C\u610F.png")));
		confirmButton.setBounds(131, 193, 93, 23);
		getContentPane().add(confirmButton);
	}
	/**
	 * 修改密码
	 * @param account
	 * @param userJFrame
	 */
	protected void changedPasswordAction(Account account, JFrame userJFrame) {
		Icon icon = new ImageIcon(getClass().getResource("/picture/疑惑.png"));
		Icon iconOfTrue = new ImageIcon(getClass().getResource("/picture/笑脸.png"));
		Icon iconOfFalse = new ImageIcon(getClass().getResource("/picture/难过.png"));
		//获取输入框中的内容
		String newPassword = new String(newPasswordTxt.getPassword());
		String confirmPassword = new String(confirmPasswordTxt.getPassword());
		//判断文本框是否为空
		if(newPassword.equals("")){
			JOptionPane.showMessageDialog(null, "您还没填入要修改的密码呀", "提示", JOptionPane.INFORMATION_MESSAGE, icon);
		}else if((!newPassword.equals(""))&&confirmPassword.equals("")) {
			JOptionPane.showMessageDialog(null, "要确认密码！", "提示", JOptionPane.WARNING_MESSAGE);
		}else {
			//判断两次输入的密码是否一致
			if(newPassword.equals(confirmPassword)) {
				User user = new User(account.getId(), confirmPassword);
				boolean result = dao.changePassword(user);
				if(result) {
					JOptionPane.showMessageDialog(null, "修改成功！", "提示信息", JOptionPane.INFORMATION_MESSAGE, iconOfTrue);
					JOptionPane.showMessageDialog(null, "请重新登录！", "提示信息",JOptionPane.INFORMATION_MESSAGE , iconOfTrue);
					this.dispose();
					userJFrame.dispose();
					new LoginJFrame().setVisible(true);
				}else {
					JOptionPane.showMessageDialog(null, "修改失败", "警告", JOptionPane.INFORMATION_MESSAGE, iconOfFalse);
				}
			}else {
				JOptionPane.showMessageDialog(null, "请重写确认您的新密码", "警告", JOptionPane.INFORMATION_MESSAGE, iconOfFalse);
			}
		}
		
	}
}
