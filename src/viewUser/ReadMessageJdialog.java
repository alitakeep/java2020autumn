package viewUser;

import java.awt.BorderLayout;
import java.awt.Color;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.RenewBookDao;
import model.RenewBook;
import model.User;
import util.GetMessage;
import util.RoundButton;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class ReadMessageJdialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private RenewBookDao dao = new RenewBookDao();
	/**
	 * Create the dialog.
	 */
	public ReadMessageJdialog(User user) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ReadMessageJdialog.class.getResource("/picture/\u67E5\u770B\u4FE1\u606F.png")));
		setBounds(100, 100, 450, 340);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 436, 222);
		contentPanel.add(scrollPane);
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setForeground(new Color(0, 0, 0));
		textArea.setLineWrap(true);
		textArea.setFont(new Font("楷体", Font.PLAIN, 15));
		
		fillArea(user,textArea);
		scrollPane.setViewportView(textArea);
		
		JButton confirmButton = new RoundButton("确认", 1);
		//JButton confirmButton = new JButton("\u786E\u5B9A");
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmAction(user);
			}
		});
		confirmButton.setBackground(SystemColor.controlLtHighlight);
		confirmButton.setIcon(new ImageIcon(ReadMessageJdialog.class.getResource("/picture/\u540C\u610F.png")));
		confirmButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		confirmButton.setBounds(169, 258, 93, 23);
		contentPanel.add(confirmButton);
		
	}
/**
 * 当点击确定按钮时，执行该方法
 * @param user
 */
	private void confirmAction(User user) {
		//确认后删除数据库中有关该用户提交的申请信息
		dao.delectMessage(new RenewBook(user.getId()));
		//关闭该窗口
		ReadMessageJdialog.this.dispose();
	}
	
	private void fillArea(User user,JTextArea textArea) {
		Vector<RenewBook> renewBooks = dao.seach(user.getId());
		StringBuffer[] message = GetMessage.getMessage(renewBooks);
		for(StringBuffer text:message) {
			if(!text.toString().equals("")) {
				textArea.append(text.toString()+"\r\n");
			}
		}
	}
}
