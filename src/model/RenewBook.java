package model;

public class RenewBook {
	private String id;
	private String idOfBook;
	private String reply;
	/*
	 * 以下两个变量并不存在于相应的数据库表中。但为了便于数据的传递（具体可见RenewBookDao中的search（String id）方法，更好的体现封装性这一思想
	 * 在该处将其定义为该类的成员变量
	 */
	private int numberOfRenew;
	private String bookName;
	
	public RenewBook(String id) {
		super();
		this.id = id;
	}
	public RenewBook() {
		// TODO 自动生成的构造函数存根
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public int getNumberOfRenew() {
		return numberOfRenew;
	}
	public void setNumberOfRenew(int numberOfRenew) {
		this.numberOfRenew = numberOfRenew;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdOfBook() {
		return idOfBook;
	}
	public void setIdOfBook(String idOfBook) {
		this.idOfBook = idOfBook;
	}
	
}
