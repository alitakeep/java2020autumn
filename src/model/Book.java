package model;

import java.sql.Blob;
import java.sql.Clob;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;

/**
 * 该类是图书信息表的实体类
 * @author 10432
 *
 */
public class Book {
	public String bookName;
	public String bookNumber;
	public String bookType;
	public String bookCase;
	public String author;
	public SerialBlob photo;
	public SerialClob introduction;

	
	public SerialBlob getPhoto() {
		return photo;
	}
	public void setPhoto(SerialBlob photo) {
		this.photo = photo;
	}
	public SerialClob getIntroduction() {
		return introduction;
	}
	public void setIntroduction(SerialClob introduction) {
		this.introduction = introduction;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getBookNumber() {
		return bookNumber;
	}
	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}
	public String getBookType() {
		return bookType;
	}
	public void setBookType(String bookType) {
		this.bookType = bookType;
	}
	public String getBookCase() {
		return bookCase;
	}
	public void setBookCase (String bookCase) {
		this.bookCase = bookCase;
	}

}
