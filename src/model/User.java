package model;
/**
 * 该类为User类的实体类，其属性与User表中的属性相对应
 * @author 10432
 * @param role  为User表中的身份信息
 */
public class User {
	private String id;
	private String password;
	private String role;
	public User() {
		
	}
	public User(String id,String password) {
		super();
		this.id = id;
		this.password = password;
	}
	public User(String id,String password,String role) {
		super();
		this.id = id;
		this.password = password;
		this.role = role;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRole() {
		return role;
	}
	/**
	 * 该方法用于判断该类封装的账号和密码是否为空
	 * @return
	 */
	public boolean isEmptyOrNot() {
		if(id==""||password=="") {
			return true;
		}
		else {
			return false;
		}
	}
}
