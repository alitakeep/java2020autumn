package model;

public class Account {
	public String id;
	public String name;
	public String gender;
	public String eMail;
	public long phone;
	public Account() {
		// TODO 自动生成的构造函数存根
	}
	public Account(String id, String name, String gender, String eMail, long phone) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.eMail = eMail;
		this.phone = phone;
	}

	public Account(String id) {
		super();
		this.id = id;
		//初始化其他变量的值
		name = "";
		gender = "男";
		eMail = "";
		phone = 0L;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
}
