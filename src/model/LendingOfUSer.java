package model;
/**
 * 该类是借书表的实体类
 * @author 10432
 *
 */

import java.sql.Date;

public class LendingOfUSer {
	public String idOfBook;
	public String idOfBorrower;
	public Date dateOfBorrowed;
	public Date dateOfReturn;
	public int numberOfRenew;
	public int getNumberOfRenew() {
		return numberOfRenew;
	}
	public void setNumberOfRenew(int numberOfRenew) {
		this.numberOfRenew = numberOfRenew;
	}
	public String getIdOfBook() {
		return idOfBook;
	}
	public void setIdOfBook(String idOfBook) {
		this.idOfBook = idOfBook;
	}
	public String getIdOfBorrower() {
		return idOfBorrower;
	}
	public void setIdOfBorrower(String idOfBorrower) {
		this.idOfBorrower = idOfBorrower;
	}
	public Date getDateOfBorrowed() {
		return dateOfBorrowed;
	}
	public void setDateOfBorrowed(Date dateOfBorrowed) {
		this.dateOfBorrowed = dateOfBorrowed;
	}
	public Date getDateOfReturn() {
		return dateOfReturn;
	}
	public void setDateOfReturn(Date dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}
	public LendingOfUSer() {}
	public LendingOfUSer(String idOfBook, int numberOfRenew) {
		super();
		this.idOfBook = idOfBook;
		this.numberOfRenew = numberOfRenew;
	}

}
