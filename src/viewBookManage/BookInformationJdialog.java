package viewBookManage;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dao.BookDao;
import model.Book;
import util.ChangeBackground;

import util.IsTextEmptyOrNot;
import util.PackagingTabletextInTextField;
import viewUser.IntroductionOfBookJdialog;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.Map;
import java.util.Vector;
import java.awt.Color;

import javax.swing.JComboBox;

import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class BookInformationJdialog extends JDialog {
	private JTextField bookNameTxt;
	private JTextField changedBookNumberTxt;
	private JTextField changedBookNameTxt;
	private JTextField changedBookTypeTxt;
	private JTextField changedBookCaseTxt;
	private JTextField changedAuthorTxt;
	private JTable table;
	
	private BookDao dao = new BookDao();
	private Vector<Vector<String>> bookInformation = new Vector<Vector<String>>();
	private Vector<JTextField> vectorOfTextFields = new Vector<JTextField>();
	/**
	 * Create the dialog.
	 */
	public BookInformationJdialog(Vector data,Vector column) {
		setBounds(100, 100, 731, 518);
		getContentPane().setLayout(new BorderLayout());
		ImageIcon icon = new ImageIcon(getClass().getResource("/picture/布拉格的修道院.jpg"));
		JPanel contentPanel = new ChangeBackground(icon);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 59, 719, 164);
		contentPanel.add(scrollPane);
		
		table = new JTable();
		table.setEnabled(true);
		table.setRowSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(false);
		scrollPane.setViewportView(table);
		
		bookNameTxt = new JTextField();
		bookNameTxt.setBounds(328, 28, 155, 21);
		contentPanel.add(bookNameTxt);
		bookNameTxt.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("\u4E66\u7C4D\u540D\u79F0\uFF1A");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("微软雅黑", Font.PLAIN, 16));
		lblNewLabel.setBounds(251, 31, 103, 15);
		contentPanel.add(lblNewLabel);
		
		JButton searchButton = new JButton("\u641C\u7D22");
		//处理查询事件
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!bookNameTxt.getText().equals("")) {
					bookInformation = dao.searchBook(bookNameTxt.getText());
				}else {
					bookInformation = dao.searchAllBook().get(1);
				}
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setDataVector(bookInformation, column);
			}
		});
		searchButton.setBackground(new Color(255, 255, 255));
		searchButton.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u67E5\u8BE2.png")));
		searchButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		searchButton.setBounds(536, 22, 103, 28);
		contentPanel.add(searchButton);
		
		JLabel lblNewLabel_6 = new JLabel("\u5355\u51FB\u4EE5\u83B7\u53D6\u6240\u9009\u884C\u7684\u4FE1\u606F");
		lblNewLabel_6.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u8B66\u544A.png")));
		lblNewLabel_6.setForeground(Color.YELLOW);
		lblNewLabel_6.setFont(new Font("华文楷体", Font.BOLD, 16));
		lblNewLabel_6.setBounds(24, 233, 230, 35);
		contentPanel.add(lblNewLabel_6);
		//为刷新按钮添加监听器
		JButton refreshButton = new JButton("\u5237\u65B0");
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map<Integer, Vector> newMap = dao.searchAllBook();
				DefaultTableModel model = (DefaultTableModel)BookInformationJdialog.this.table.getModel();
				model.setDataVector(newMap.get(1), newMap.get(2));
				//清空文本框中内容
				for(JTextField textField:vectorOfTextFields) {
					textField.setText("");
				}
			}
		});
		refreshButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		refreshButton.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u5237\u65B0.png")));
		refreshButton.setBounds(519, 233, 93, 31);
		contentPanel.add(refreshButton);
		
		JButton changedButton = new JButton("\u4FEE\u6539");
		//为修改按钮添加监听器
		changedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeActionPerform();
			}
		});
		changedButton.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u4FEE\u6539.png")));
		changedButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		changedButton.setBackground(new Color(240, 255, 255));
		changedButton.setBounds(599, 288, 95, 28);
		contentPanel.add(changedButton);
		//为增添按钮添加监视器
		JButton addBookButton = new JButton("\u589E\u6DFB\r\n");
		addBookButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AddBookJdialog().setVisible(true);
			}
		});
		addBookButton.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u52A0\u53F7.png")));
		addBookButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		addBookButton.setBackground(new Color(255, 250, 250));
		addBookButton.setBounds(599, 343, 95, 28);
		contentPanel.add(addBookButton);
		
		JButton deletedButton = new JButton("\u5220\u9664");
		//为删除事件添加监听器
		deletedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delectActionPerformed();
			}
		});
		deletedButton.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u5220\u9664-1.png")));
		deletedButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		deletedButton.setBackground(new Color(255, 250, 240));
		deletedButton.setBounds(599, 393, 95, 28);
		contentPanel.add(deletedButton);
		
		JLabel lblNewLabel_3 = new JLabel("\u4E66\u7C4D\u7F16\u53F7\uFF1A");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_3.setBackground(Color.WHITE);
		lblNewLabel_3.setBounds(56, 288, 96, 21);
		contentPanel.add(lblNewLabel_3);
		
		changedBookNumberTxt = new JTextField();
		changedBookNumberTxt.setEditable(false);
		changedBookNumberTxt.setColumns(10);
		changedBookNumberTxt.setBounds(137, 290, 141, 21);
		contentPanel.add(changedBookNumberTxt);
		
		JLabel lblNewLabel_1 = new JLabel("\u4E66\u540D\uFF1A");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_1.setBounds(68, 339, 54, 15);
		contentPanel.add(lblNewLabel_1);
		
		changedBookNameTxt = new JTextField();
		changedBookNameTxt.setEditable(false);
		changedBookNameTxt.setColumns(10);
		changedBookNameTxt.setBounds(137, 338, 132, 21);
		contentPanel.add(changedBookNameTxt);
		
		changedBookTypeTxt = new JTextField();
		changedBookTypeTxt.setEnabled(false);
		changedBookTypeTxt.setColumns(10);
		changedBookTypeTxt.setBounds(137, 383, 75, 21);
		contentPanel.add(changedBookTypeTxt);
		
		JLabel lblNewLabel_4 = new JLabel("\u4E66\u7C4D\u7C7B\u578B\uFF1A");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_4.setBounds(43, 386, 98, 15);
		contentPanel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_2 = new JLabel("\u5728\u9986\u4E0E\u5426\uFF1A");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_2.setBounds(328, 295, 91, 15);
		contentPanel.add(lblNewLabel_2);
		
		changedBookCaseTxt = new JTextField();
		changedBookCaseTxt.setEditable(false);
		changedBookCaseTxt.setColumns(10);
		changedBookCaseTxt.setBounds(413, 294, 134, 21);
		contentPanel.add(changedBookCaseTxt);
		
		JLabel lblNewLabel_7 = new JLabel("\u4F5C\u8005\uFF1A");
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_7.setBounds(344, 339, 70, 23);
		contentPanel.add(lblNewLabel_7);
		
		changedAuthorTxt = new JTextField();
		changedAuthorTxt.setEditable(false);
		changedAuthorTxt.setBounds(413, 339, 131, 21);
		contentPanel.add(changedAuthorTxt);
		changedAuthorTxt.setColumns(10);
		
		//填充表格数据
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setDataVector(data, column);
		//把文本框封装进向量vectorOfTextFields中
		packagingTextIntoVector();
		//为表格添加鼠标事件
		table.addMouseListener(new PackagingTabletextInTextField(this.table,vectorOfTextFields));
		
		JButton LendingBookButton = new JButton("\u56FE\u4E66\u501F\u51FA");
		//为借书按钮添加监听器
		LendingBookButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				lendingActionPerformed(LendingBookButton);
			}
		});
		LendingBookButton.setIcon(new ImageIcon(BookInformationJdialog.class.getResource("/picture/\u501F\u51FA.png")));
		LendingBookButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		LendingBookButton.setBackground(new Color(245, 245, 245));
		LendingBookButton.setBounds(418, 431, 129, 35);
		contentPanel.add(LendingBookButton);
		
		JButton introductionButton = new JButton("\u8BE6\u60C5");
		introductionButton.setForeground(SystemColor.desktop);
		introductionButton.setBackground(new Color(255, 255, 255));
		//为详情按钮添加监听器
		introductionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				introducingActionPerform();
			}
		});
		
		introductionButton.setBounds(305, 241, 93, 23);
		contentPanel.add(introductionButton);
	}
	
	
	
	/**
	 * 创建一个相关图书的介绍的窗口
	 */
	private void introducingActionPerform() {
		//获取对应图书的封面图片
		ImageIcon icon = dao.getImageIcon(changedBookNumberTxt.getText());
		//获取对应图书的内容简介
		String text = dao.getArticle(changedBookNumberTxt.getText());
		new IntroductionOfBookJdialog(icon,text).setVisible(true);
	}




	private void lendingActionPerformed(JButton LendingBookButton) {
		//当没有选中书籍时，弹出提示框
		if(changedBookNameTxt.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "请先点击表格中的某一行以获取相应的图书信息", "警告", JOptionPane.WARNING_MESSAGE);
		}else {
			new LendingBookJdialog(changedBookNumberTxt.getText()).setVisible(true);
		}
		
	}
	/**
	 * 该方法用于处理删除事件
	 */
	private void delectActionPerformed() {
		Book book = packagingTextIntoBook();
		if(book.getBookCase().equals("在馆")) {
			boolean result = dao.delectBookInformation(book);
			if(result) {
				JOptionPane.showMessageDialog(null, "修改成功");	
			}
			else {
				JOptionPane.showMessageDialog(null, "修改失败");
			}
		}else {
			JOptionPane.showMessageDialog(null, "图书借出状态下不能删除");
		}
	}
	
	/**
	 * 该方法用于处理修改事件
	 */
	private void changeActionPerform() {
		IsTextEmptyOrNot isTextEmptyOrNot = new IsTextEmptyOrNot();
		boolean haveName = isTextEmptyOrNot.isTheTextEmptyOrNot(changedBookNameTxt, "图书名称");
		if(haveName) {
			Book book = dao.getBook(changedBookNumberTxt.getText());
			new ChangeBookInformationJdialog(book).setVisible(true);
		}
	}
	/**
	 * 该方法用于把文本框中的信息封装到Book类中
	 * @return	model.Book
	 */
	private Book packagingTextIntoBook() {
		Book book = new Book();
		book.setBookNumber(changedBookNumberTxt.getText());
		book.setBookName(changedBookNameTxt.getText());
		book.setBookType(changedBookTypeTxt.getText());
		book.setBookCase(changedBookCaseTxt.getText());
		book.setAuthor(changedAuthorTxt.getText());
		return book;
	}
	/**
	 * 该方法用于把文本框按顺序装入向量中
	 */
	private void packagingTextIntoVector() {
		vectorOfTextFields.add(changedBookNumberTxt);
		vectorOfTextFields.add(changedBookNameTxt);
		vectorOfTextFields.add(changedBookTypeTxt);
		vectorOfTextFields.add(changedBookCaseTxt);
		vectorOfTextFields.add(changedAuthorTxt);
		
	}
}
