package viewBookManage;

import java.awt.BorderLayout;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import javax.swing.border.TitledBorder;

import dao.BookDao;
import model.Book;
import util.ChangeBackground;

import util.IsTextEmptyOrNot;
import util.PhotoOrArticle;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;

import java.sql.Blob;

import java.awt.event.ActionEvent;

import javax.sql.rowset.serial.SerialBlob;

import javax.swing.DefaultComboBoxModel;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class AddBookJdialog extends JDialog {

	private BookDao dao;
	private IsTextEmptyOrNot isTextEmptyOrNot;	
	private JPanel contentPanel = new JPanel();
	private JTextField BookIdTxt;
	private JTextField BookNameTxt;
	private JComboBox<String> BookTypeCBox;
	private JTextField BookAuthorTxt;
	//用于展示图片信息
	private JLabel photoJLable;
	//用于封装图片信息
	private SerialBlob blob;
	private JTextArea introduceOfBookArea;
	
	private PhotoOrArticle pa = new PhotoOrArticle();
	/**
	 * Create the dialog.
	 */
	public AddBookJdialog() {
		setBounds(100, 100, 714, 557);
		contentPanel.setLayout(new BorderLayout());
		ImageIcon icon = new ImageIcon(AddBookJdialog.class.getResource("/picture/七彩板.jpg"));
		ChangeBackground jpanel = new ChangeBackground(icon);
		//JPanel jpanel = new ChangeBackground(icon);
		contentPanel.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		BookIdTxt = new JTextField();
		BookIdTxt.setBounds(104, 49, 125, 21);
		jpanel.add(BookIdTxt);
		BookIdTxt.setColumns(10);
		
		BookNameTxt = new JTextField();
		BookNameTxt.setColumns(10);
		BookNameTxt.setBounds(104, 92, 125, 21);
		jpanel.add(BookNameTxt);
		
		JLabel lblNewLabel = new JLabel("\u56FE\u4E66\u7F16\u53F7\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(23, 51, 90, 15);
		jpanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u4E66\u7C4D\u540D\u79F0\uFF1A\r\n");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(23, 95, 90, 15);
		jpanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u56FE\u4E66\u7C7B\u522B\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(23, 141, 90, 15);
		jpanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u4F5C\u8005\uFF1A");
		lblNewLabel_3.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_3.setBounds(23, 186, 90, 15);
		jpanel.add(lblNewLabel_3);
		
		BookTypeCBox = new JComboBox<String>();
		BookTypeCBox.setModel(new DefaultComboBoxModel(new String[] {"\u5DE5\u5177\u7C7B", "\u4EBA\u6587\u793E\u79D1\u7C7B", "\u6587\u5B66\u7C7B"}));
		BookTypeCBox.setBounds(104, 139, 99, 23);
		jpanel.add(BookTypeCBox);
		
		JButton addBookInformationButton = new JButton("\u6DFB\u52A0");
		addBookInformationButton.setIcon(new ImageIcon(AddBookJdialog.class.getResource("/picture/\u589E\u52A0.png")));
		addBookInformationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addBookInformation();
								
			}
		});
		addBookInformationButton.setBackground(Color.PINK);
		addBookInformationButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		addBookInformationButton.setBounds(438, 394, 107, 30);
		jpanel.add(addBookInformationButton);
		
		BookAuthorTxt = new JTextField();
		BookAuthorTxt.setBounds(104, 185, 119, 21);
		jpanel.add(BookAuthorTxt);
		BookAuthorTxt.setColumns(10);
		
		JButton uploadPhotoButton = new JButton("\u4E0A\u4F20\u5C01\u9762");
		uploadPhotoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				blob = pa.setPhoto();
				photoJLable.setIcon(pa.setImage(blob));
			}
		});
		uploadPhotoButton.setFont(new Font("华文楷体", Font.PLAIN, 13));
		uploadPhotoButton.setForeground(UIManager.getColor("Button.disabledShadow"));
		uploadPhotoButton.setBackground(Color.WHITE);
		uploadPhotoButton.setBounds(462, 320, 93, 23);
		jpanel.add(uploadPhotoButton);
		
		photoJLable = new JLabel("");
		photoJLable.setBounds(369, 10, 300, 300);
		jpanel.add(photoJLable);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 234, 311, 243);
		jpanel.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "图书简介", TitledBorder.CENTER,TitledBorder.TOP, new Font("华文楷体", Font.PLAIN, 18),Color.black));
		scrollPane.setViewportView(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		introduceOfBookArea = new JTextArea();
		introduceOfBookArea.setLineWrap(true);
		panel.add(introduceOfBookArea, BorderLayout.CENTER);
		this.setContentPane(contentPanel);
	}
	
	/**
	 * 该方法用于处理增添事件
	 */
	private void addBookInformation() {
		isTextEmptyOrNot = new IsTextEmptyOrNot();
		boolean haveId = isTextEmptyOrNot.isTheTextEmptyOrNot(BookIdTxt, "图书编号");
		boolean haveName = isTextEmptyOrNot.isTheTextEmptyOrNot(BookNameTxt, "图书名称");
		//判断输入的图书编号和图书名称是否为空，不是则执行增添
		if(haveId&&haveName) {
			Book book = getTextInJdialog();
			dao = new BookDao();
			boolean result = dao.addBooks(book);
			if(result) {
				JOptionPane.showMessageDialog(null, "添加成功");
				this.dispose();
			}
			else {
				JOptionPane.showMessageDialog(null, "添加失败");
			}
		}
	}
	/**
	 * 该方法用于获取输入框中的内容
	 */
	private Book getTextInJdialog() {
		Book book = new Book();
		book.setBookNumber(BookIdTxt.getText());
		book.setBookName(BookNameTxt.getText());
		book.setBookType(BookTypeCBox.getSelectedItem().toString());
		book.setAuthor(BookAuthorTxt.getText());
		//防止没有上传图片时执行操作后空指针报错
		if(blob!=null) {
			book.setPhoto(blob);
		}
		book.setIntroduction(pa.getClob(introduceOfBookArea.getText()));
		return book;
	}
}
