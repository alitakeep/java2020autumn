package viewBookManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Vector;


import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dao.LendingOfUserDao;
import model.LendingOfUSer;
import util.ChangeBackground;

import util.IsTextEmptyOrNot;
import util.PackagingTabletextInTextField;
import javax.swing.UIManager;


public class ReturnBookManageJdialog extends JDialog {
	
	private  JPanel contentPanel;
	private JTextField idOfBookTxt;
	private JTextField idOfBorrowerTxt;
	private JTable table;
	private JTextField changedIdOfBookTxt;
	private JTextField changedIdOfBorrowerTxt;
	private JTextField changedDateTxt;
	private JTextField changedNumOfRenewTxt;
	private JLabel lblNewLabel_5;

	private Vector<JTextField > vectorOfTextFields = new Vector<JTextField>();
	private LendingOfUserDao dao = new LendingOfUserDao();
	private LendingOfUSer lendingOfUSer = new LendingOfUSer();
	private IsTextEmptyOrNot isTextEmptyOrNot = new IsTextEmptyOrNot();
	private JTextField changedReturnDateTxt;
	
	
	

	/**
	 * Create the dialog.
	 */
	public ReturnBookManageJdialog(Vector data,Vector<String> column) {
		//更换背景图片
		ImageIcon icon = new ImageIcon(ReturnBookManageJdialog.class.getResource("/picture/图书背景.jpg"));
		contentPanel = new ChangeBackground(icon);
		setBounds(100, 100, 677, 552);
		getContentPane().setLayout(new BorderLayout(0, 0));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		
		table = new JTable();
		table.setEnabled(true);
		table.setRowSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 101, 663, 193);
		
		contentPanel.add(scrollPane);
		//将表格添加到JScrollPane中
		scrollPane.setViewportView(table);
		
	
		idOfBookTxt = new JTextField();
		idOfBookTxt.setBounds(269, 23, 150, 21);
		contentPanel.add(idOfBookTxt);
		idOfBookTxt.setColumns(10);
		
		idOfBorrowerTxt = new JTextField();
		idOfBorrowerTxt.setColumns(10);
		idOfBorrowerTxt.setBounds(268, 68, 150, 21);
		contentPanel.add(idOfBorrowerTxt);
		
		JLabel lblNewLabel = new JLabel("\u56FE\u4E66\u7F16\u53F7\uFF1A");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("华文楷体", Font.BOLD, 16));
		lblNewLabel.setBounds(186, 24, 89, 15);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u501F\u9605\u8005\u8D26\u53F7\uFF1A");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		lblNewLabel_1.setBounds(158, 69, 100, 15);
		contentPanel.add(lblNewLabel_1);
		//为查询按钮添加监听器
		JButton searchButton = new JButton("\u67E5\u8BE2");
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vector data =searchActionPerformed();
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setDataVector(data, column);
			}
		});
		searchButton.setIcon(new ImageIcon(ReturnBookManageJdialog.class.getResource("/picture/\u67E5\u8BE2.png")));
		searchButton.setBackground(new Color(240, 255, 240));
		searchButton.setForeground(Color.BLACK);
		searchButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		searchButton.setBounds(492, 35, 100, 33);
		contentPanel.add(searchButton);
		
		changedIdOfBookTxt = new JTextField();
		changedIdOfBookTxt.setEditable(false);
		changedIdOfBookTxt.setBounds(120, 377, 118, 21);
		contentPanel.add(changedIdOfBookTxt);
		changedIdOfBookTxt.setColumns(10);
		
		changedIdOfBorrowerTxt = new JTextField();
		changedIdOfBorrowerTxt.setEditable(false);
		changedIdOfBorrowerTxt.setColumns(10);
		changedIdOfBorrowerTxt.setBounds(355, 377, 118, 21);
		contentPanel.add(changedIdOfBorrowerTxt);
		
		JLabel lblNewLabel_2 = new JLabel("\u56FE\u4E66\u7F16\u53F7\uFF1A");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_2.setBounds(43, 377, 85, 21);
		contentPanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u501F\u9605\u4EBA\u8D26\u53F7\uFF1A");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_3.setBounds(248, 378, 105, 15);
		contentPanel.add(lblNewLabel_3);
		
		changedDateTxt = new JTextField();
		changedDateTxt.setEditable(false);
		changedDateTxt.setBounds(120, 429, 122, 21);
		contentPanel.add(changedDateTxt);
		changedDateTxt.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("\u501F\u4E66\u65E5\u671F\uFF1A");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_4.setBounds(40, 430, 84, 21);
		contentPanel.add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("\u5355\u51FB\u8868\u683C\u4E2D\u7684\u67D0\u4E00\u884C\u4EE5\u83B7\u53D6\u5176\u6570\u636E");
		lblNewLabel_5.setIcon(new ImageIcon(ReturnBookManageJdialog.class.getResource("/picture/\u8B66\u544A.png")));
		lblNewLabel_5.setFont(new Font("华文行楷", Font.PLAIN, 18));
		lblNewLabel_5.setForeground(new Color(176, 224, 230));
		lblNewLabel_5.setBounds(0, 336, 333, 26);
		contentPanel.add(lblNewLabel_5);
		
		//为归还图书按钮添加监听器
		JButton returnBookButton = new JButton("\u5F52\u8FD8\u56FE\u4E66");
		returnBookButton.setForeground(new Color(0, 0, 0));
		returnBookButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				returnActionPerformed();
			}
		});
		returnBookButton.setIcon(new ImageIcon(ReturnBookManageJdialog.class.getResource("/picture/\u8FD8\u4E66.png")));
		returnBookButton.setBackground(new Color(250, 250, 210));
		returnBookButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		returnBookButton.setBounds(522, 331, 131, 35);
		contentPanel.add(returnBookButton);
		
		JButton refreshButton = new JButton("\u5237\u65B0\r\n");
		//为刷新按钮添加监听器
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map<Integer, Vector> newMap = dao.searchAll();
				DefaultTableModel newModel = (DefaultTableModel) ReturnBookManageJdialog.this.table.getModel();
				newModel.setDataVector(newMap.get(1), column);
				//清空文本框中的内容
				for(JTextField textField:vectorOfTextFields) {
					textField.setText("");
				}
			}
		});
		refreshButton.setBackground(UIManager.getColor("Button.light"));
		refreshButton.setIcon(new ImageIcon(ReturnBookManageJdialog.class.getResource("/picture/\u5237\u65B0.png")));
		refreshButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		refreshButton.setBounds(269, 303, 93, 33);
		contentPanel.add(refreshButton);
		
		JLabel lblNewLabel_6 = new JLabel("\u7EED\u501F\u6B21\u6570\uFF1A");
		lblNewLabel_6.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setBounds(310, 471, 89, 15);
		contentPanel.add(lblNewLabel_6);
		
		changedNumOfRenewTxt = new JTextField();
		changedNumOfRenewTxt.setEditable(false);
		changedNumOfRenewTxt.setFont(new Font("微软雅黑", Font.PLAIN, 16));
		changedNumOfRenewTxt.setBounds(385, 470, 54, 21);
		contentPanel.add(changedNumOfRenewTxt);
		changedNumOfRenewTxt.setColumns(10);
		
		
		JLabel lblNewLabel_7 = new JLabel("\u5F52\u8FD8\u65E5\u671F\uFF1A");
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setFont(new Font("微软雅黑", Font.BOLD, 16));
		lblNewLabel_7.setBounds(37, 469, 84, 24);
		contentPanel.add(lblNewLabel_7);
		
		changedReturnDateTxt = new JTextField();
		changedReturnDateTxt.setEditable(false);
		changedReturnDateTxt.setBounds(122, 470, 118, 21);
		contentPanel.add(changedReturnDateTxt);
		changedReturnDateTxt.setColumns(10);
		//初始化表格组件
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setDataVector(data, column);
		//把文本框封装进向量中
		vectorOfTextFields=packagingTextIntoVector();
		//为表格添加监听器
		table.addMouseListener(new PackagingTabletextInTextField(this.table, vectorOfTextFields));
	}
	
	/**
	 * 归还图书按钮的实现方法
	 */
	private void returnActionPerformed() {
		if(isTextEmptyOrNot.isTheTextEmptyOrNot(changedIdOfBookTxt)) {
			boolean result = dao.returnBook(packagingTextIntoLendingOfUSer());
			if(result) {
				JOptionPane.showMessageDialog(null, "修改成功！");
			}
			else {
				JOptionPane.showMessageDialog(null, "修改失败", "警告", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(null, "请选中要操作的信息所在行", "警告", JOptionPane.WARNING_MESSAGE);
		}
	}
	/**
	 * 该方法用于生成查询的sql语句
	 * @return	返回一个封装了查询数据的向量集
	 */
	private Vector searchActionPerformed() {
		StringBuffer sql = new StringBuffer("select * from lending_information ");
		//判断文本框中的内容是否为空
		boolean haveIdOfBook = isTextEmptyOrNot.isTheTextEmptyOrNot(idOfBookTxt);
		boolean haveIdOfBorrower = isTextEmptyOrNot.isTheTextEmptyOrNot(idOfBorrowerTxt);
		//若只输入了图书编号，则查询相应图书的信息
		if(haveIdOfBook==true&&haveIdOfBorrower==false) {
			sql.append("where idOfBook= '"+idOfBookTxt.getText()+"'");
		}
		//若只输入了借阅者账号，则查询相应借阅者的借阅信息
		else if(haveIdOfBook==false&&haveIdOfBorrower==true) {
			sql.append("where idOfBorrower= '"+idOfBorrowerTxt.getText()+"'");
		}
		//如果都输入则按所给的两个条件进行查询
		else if(haveIdOfBook&&haveIdOfBorrower) {
			sql.append("where idOfBook= '"+idOfBookTxt.getText()+ "' and idOfBorrower= '"+idOfBorrowerTxt.getText()+"'");
		}
		else {
			JOptionPane.showMessageDialog(null, "请输入书籍编号或借阅者账号", "警告",JOptionPane.WARNING_MESSAGE);
		}
		Vector data = dao.search(sql.toString());
		return data;
		
	}
	/**
	 * 该方法用于把文本框按顺序装入向量中
	 */
	private Vector<JTextField> packagingTextIntoVector() {
		Vector<JTextField> vector = new Vector<JTextField>();
		vector.add(changedIdOfBorrowerTxt);
		vector.add(changedIdOfBookTxt);
		vector.add(changedDateTxt);
		vector.add(changedNumOfRenewTxt);
		vector.add(changedReturnDateTxt);
		return vector;
	}
	/**
	 * 将文本框中的数据封装到LendingOfUser类中
	 * @return 返回一个封装了借书信息的实体类
	 */
	private LendingOfUSer packagingTextIntoLendingOfUSer(){
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		/*由于java.sql.Date和java.util.Date没有提供相应的转换方法，所以此处通过SimpleDateFormat类中的方法先将对应的日期字符
		 * 转化成java.util.Date类，然后用java.util.Date类获取自1970年1月1日以来，由此 Date对象表示的00:00:00 GMT的毫秒 数 。
		 * 继而用该值构造出一个java.sql.Date 类
		*/
		java.util.Date uDate = new java.util.Date();
		try {
			uDate = sf.parse(changedDateTxt.getText());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		Date sDate = new Date(uDate.getTime());
		lendingOfUSer.setIdOfBook(changedIdOfBookTxt.getText());
		lendingOfUSer.setIdOfBorrower(changedIdOfBorrowerTxt.getText());
		lendingOfUSer.setDateOfBorrowed(sDate);
		lendingOfUSer.setNumberOfRenew(Integer.parseInt(changedNumOfRenewTxt.getText()));
		return lendingOfUSer;
	}
}
