package viewBookManage;

import java.awt.BorderLayout;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JComboBox;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.TitledBorder;

import dao.BookDao;
import model.Book;
import util.PhotoOrArticle;

import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Blob;
import java.sql.SQLException;
import java.awt.event.ActionEvent;


public class ChangeBookInformationJdialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField bookNameTxt;
	private JTextField authorTxt;
	private JComboBox bookTypeCBox;
	private JTextArea introduceOfBookArea;
	private JLabel photoJLable;
	private SerialBlob blob;
	private PhotoOrArticle pa = new PhotoOrArticle();
	private BookDao dao = new BookDao();
	/**
	 * Create the dialog.
	 */
	public ChangeBookInformationJdialog(Book book) {
		setBounds(100, 100, 685, 497);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			bookNameTxt = new JTextField();
			bookNameTxt.setBounds(121, 50, 158, 21);
			bookNameTxt.setColumns(10);
			contentPanel.add(bookNameTxt);
		}
		
		authorTxt = new JTextField();
		authorTxt.setColumns(10);
		authorTxt.setBounds(121, 102, 158, 21);
		contentPanel.add(authorTxt);
		
		JLabel lblNewLabel = new JLabel("\u4E66\u540D\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(55, 53, 54, 15);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u4F5C\u8005\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(54, 107, 61, 15);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("\u4E66\u7C4D\u7C7B\u578B\uFF1A");
		lblNewLabel_1_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1_1.setBounds(29, 155, 86, 15);
		contentPanel.add(lblNewLabel_1_1);
		
		bookTypeCBox = new JComboBox();
		bookTypeCBox.setModel(new DefaultComboBoxModel(new String[] {"\u4EBA\u6587\u793E\u79D1\u7C7B", "\u5DE5\u5177\u7C7B", "\u6587\u5B66\u7C7B"}));
		bookTypeCBox.setBounds(121, 153, 91, 23);
		contentPanel.add(bookTypeCBox);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "图书简介", TitledBorder.CENTER,TitledBorder.TOP, new Font("华文楷体", Font.PLAIN, 18),Color.black));
		panel.setBounds(10, 209, 309, 241);
		contentPanel.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		introduceOfBookArea = new JTextArea();
		scrollPane.setViewportView(introduceOfBookArea);
		introduceOfBookArea.setLineWrap(true);
		
		photoJLable = new JLabel("");
		photoJLable.setBounds(344, 20, 300, 300);
		contentPanel.add(photoJLable);
		
		JButton uploadPhotoButton = new JButton("\u4FEE\u6539\u5C01\u9762");
		uploadPhotoButton.addActionListener(new ActionListener() {
			//为修改图片按钮添加监听器
			public void actionPerformed(ActionEvent e) {
				//修改储存图片信息的变量
				 blob = pa.setPhoto();
				 photoJLable.setIcon(pa.setImage((SerialBlob) blob));
			}
		});
		uploadPhotoButton.setForeground(SystemColor.desktop);
		uploadPhotoButton.setFont(new Font("华文楷体", Font.PLAIN, 13));
		uploadPhotoButton.setBackground(SystemColor.window);
		uploadPhotoButton.setBounds(455, 340, 93, 23);
		contentPanel.add(uploadPhotoButton);
		
		JButton addBookInformationButton = new JButton("\u786E\u8BA4");
		//为确认添加监听器
		addBookInformationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmAction(book);
			}
		});
		addBookInformationButton.setIcon(new ImageIcon(ChangeBookInformationJdialog.class.getResource("/picture/\u540C\u610F.png")));
		addBookInformationButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		addBookInformationButton.setBackground(Color.PINK);
		addBookInformationButton.setBounds(455, 405, 107, 30);
		contentPanel.add(addBookInformationButton);
		fillAll(book);
	}
	/**
	 * 修改图书信息
	 * @param book
	 */
	protected void confirmAction(Book book) {
		Book newBook = getChangedBook(book);
		boolean result = dao.changeBookInformation(newBook);
		if (result) {
			JOptionPane.showMessageDialog(null, "修改成功！");
			this.dispose();
		}else {
			JOptionPane.showMessageDialog(null, "修改失败！","警告", JOptionPane.WARNING_MESSAGE);
		}
	}
	/**
	 * 把要修改的书籍信息填充到该界面中
	 * @param book
	 */
	private void fillAll(Book book) {
		bookNameTxt.setText(book.getBookName());
		authorTxt.setText(book.getAuthor());
		bookTypeCBox.setSelectedItem(book.getBookType());
		if(book.getPhoto()!=null) {
			photoJLable.setIcon(pa.setImage(book.getPhoto()));
			//记录当前的图片
			blob = book.getPhoto();
		}else {
			photoJLable.setText("暂无封面");
		}
		if(book.getIntroduction()!=null) {
			introduceOfBookArea.setText(pa.getArticle(book.getIntroduction()));
		}else {
			introduceOfBookArea.setText("");
		}
		
	}
	/**
	 * 在原来的实体类基础上修改信息
	 * @param book 封装了该书籍的实体类
	 * @return
	 */
	private Book getChangedBook(Book book) {
		book.setBookName(bookNameTxt.getText());
		book.setAuthor(authorTxt.getText());
		book.setBookType((String) bookTypeCBox.getSelectedItem());
		book.setIntroduction(pa.getClob(introduceOfBookArea.getText()));
		book.setPhoto(blob);
		return book;
	}
}
