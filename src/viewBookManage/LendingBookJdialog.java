package viewBookManage;


import javax.swing.JButton;
import javax.swing.JDialog;

import dao.LendingOfUserDao;
import model.LendingOfUSer;
import util.GetDateFromJCombox;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

public class LendingBookJdialog extends JDialog {
	private JTextField idOfBookTxt;
	private JTextField idOfBorrowerTxt;
	private JTextField dateOfBorrowedTxt;
	private JComboBox dayCBox = new JComboBox();
	private JComboBox monthCBox = new JComboBox();

	private LendingOfUserDao dao = new LendingOfUserDao();

	/**
	 * Create the dialog.
	 */
	public LendingBookJdialog(String bookNumber) {
		setBounds(100, 100, 482, 388);
		getContentPane().setLayout(null);
		
		idOfBookTxt = new JTextField();
		idOfBookTxt.setEnabled(false);
		idOfBookTxt.setBounds(131, 50, 131, 21);
		//给图书编号框添加默认信息
		idOfBookTxt.setText(bookNumber);
		getContentPane().add(idOfBookTxt);
		idOfBookTxt.setColumns(10);
		
		idOfBorrowerTxt = new JTextField();
		idOfBorrowerTxt.setColumns(10);
		idOfBorrowerTxt.setBounds(131, 100, 131, 21);
		getContentPane().add(idOfBorrowerTxt);
		
		dateOfBorrowedTxt = new JTextField();
		dateOfBorrowedTxt.setEnabled(false);
		dateOfBorrowedTxt.setColumns(10);
		dateOfBorrowedTxt.setBounds(131, 158, 106, 21);
		//为文本框添加当前日期
		dateOfBorrowedTxt.setText(setDefaultTime());
		
		getContentPane().add(dateOfBorrowedTxt);
		
		JLabel lblNewLabel = new JLabel("\u56FE\u4E66\u7F16\u53F7\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(41, 51, 80, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u501F\u4E66\u4EBA\u7F16\u53F7\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(10, 103, 98, 15);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u501F\u51FA\u65F6\u95F4\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(24, 161, 84, 15);
		getContentPane().add(lblNewLabel_2);
		//为借书按钮添加监听器
		JButton lendingButton = new JButton("\u501F\u51FA\r\n");
		lendingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lendingActionPerformed();
			}
		});
		lendingButton.setBackground(UIManager.getColor("Button.disabledShadow"));
		lendingButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lendingButton.setIcon(new ImageIcon(LendingBookJdialog.class.getResource("/picture/\u501F\u51FA.png")));
		lendingButton.setBounds(135, 237, 98, 32);
		getContentPane().add(lendingButton);
		
		monthCBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//获取修改后的日期的字符串表示
				String resetDate = new GetDateFromJCombox(monthCBox,  dayCBox,dateOfBorrowedTxt.getText()).getSelectedDate();
				resetDate(resetDate);
			}
		});
		monthCBox.setModel(new DefaultComboBoxModel(new String[] {"1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"}));
		monthCBox.setMaximumRowCount(6);
		monthCBox.setBounds(249, 158, 51, 23);
		getContentPane().add(monthCBox);
		dayCBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//获取修改后的日期的字符串表示
				String resetDate = new GetDateFromJCombox(monthCBox,  dayCBox,dateOfBorrowedTxt.getText()).getSelectedDate();
				resetDate(resetDate);
			}
		});
		
		
		dayCBox.setModel(new DefaultComboBoxModel(new String[] {"1\u53F7", "2\u53F7", "3\u53F7", "4\u53F7", "5\u53F7", "6\u53F7", "7\u53F7", "8\u53F7", "9\u53F7", "10\u53F7", "11\u53F7", "12\u53F7", "13\u53F7", "14\u53F7", "15\u53F7", "16\u53F7", "17\u53F7", "18\u53F7", "19\u53F7", "20\u53F7", "21\u53F7", "22\u53F7", "23\u53F7", "24\u53F7", "25\u53F7", "26\u53F7", "27\u53F7", "28\u53F7", "29\u53F7", "30\u53F7", "31\u53F7"}));
		dayCBox.setBounds(311, 158, 51, 23);
		getContentPane().add(dayCBox);
	}
	/**
	 * 该方法用于重置借书日期
	 * @param resetDate
	 */
	protected void resetDate(String resetDate) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {
			//获取要填入的日期
			date = sf.parse(resetDate);
		} catch (ParseException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}
		//获取当前日期
		java.util.Date nowTime = new java.util.Date();
		//如果选择的日期在当前日期之后，则填充当前日期
		if(nowTime.getTime()>=date.getTime()) {
			System.out.println(String.format("%tY-%<tm-%<td", date));
			dateOfBorrowedTxt.setText(resetDate);
		}else {
			System.out.println(String.format("%tY-%<tm-%<td", date));
			dateOfBorrowedTxt.setText(setDefaultTime());
		}
	}
	/**
	 * 该方法用于处理借书事件
	 */
	private void lendingActionPerformed() {
		boolean bookExsit = dao.exsitInBookDatabase(idOfBookTxt.getText());
		boolean userExsit = dao.exsitInUserDatabase(idOfBorrowerTxt.getText());
		//只有当图书在馆且用户账号正确时才执行借出操作
		if(bookExsit&&userExsit) {
			LendingOfUSer lOfUSer = packagingTextIntoLendingOfUSer();
			boolean result = dao.lendingBook(lOfUSer);
			if(result) {
				JOptionPane.showMessageDialog(null, "借书成功！");
				this.dispose();
			}else {
				JOptionPane.showMessageDialog(null, "操作失败！", "警告", JOptionPane.WARNING_MESSAGE);
			}
		}else if(bookExsit==false&&userExsit==true) {
			JOptionPane.showMessageDialog(null, "没有该图书，或该图书已被借阅", "警告", JOptionPane.WARNING_MESSAGE);
		}
		else if (bookExsit==true&&userExsit==false) {
			JOptionPane.showMessageDialog(null, "该用户不存在", "警告", JOptionPane.WARNING_MESSAGE);
		}else {
			JOptionPane.showMessageDialog(null, "没有该图书，且该用户不存在！", "警告", JOptionPane.WARNING_MESSAGE);
		}
	}
	/**
	 *	将当前日期填充到文本框中
	 */
	private String setDefaultTime() {
		java.util.Date nowtime = new java.util.Date();
		//格式化日期，使其变为YYYY-MM-DD的形式填充到表格中
		String time = String.format("%tY-%<tm-%<td", nowtime);
		return time;
	}
	
	/**
	 * 将文本框中的数据封装到LendingOfUser类中
	 */
	private LendingOfUSer packagingTextIntoLendingOfUSer(){
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		/*由于java.sql.Date和java.util.Date没有提供相应的转换方法，所以此处通过SimpleDateFormat类中的方法先将对应的日期字符
		 * 转化成java.util.Date类，然后用java.util.Date类获取自1970年1月1日以来，由此 Date对象表示的00:00:00 GMT的毫秒 数 。
		 * 继而用该值构造出一个java.sql.Date 类
		*/
		java.util.Date uDate = new java.util.Date();
		try {
			uDate = sf.parse(dateOfBorrowedTxt.getText());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		Date sDate = new Date(uDate.getTime());
		LendingOfUSer lOfUSer = new LendingOfUSer();
		lOfUSer.setIdOfBook(idOfBookTxt.getText());
		lOfUSer.setIdOfBorrower(idOfBorrowerTxt.getText());
		lOfUSer.setDateOfBorrowed(sDate);
		return lOfUSer;
	}
}
