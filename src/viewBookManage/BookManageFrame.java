package viewBookManage;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.BookDao;
import dao.LendingOfUserDao;

import model.Book;

import util.OutLog;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BookManageFrame extends JFrame {
	private OutLog outLog = new OutLog();
	private BookDao bookDao = new BookDao();
	private LendingOfUserDao lendingOfUserDao = new LendingOfUserDao();
	private JPanel contentPane;
	
	public BookManageFrame() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 687, 434);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu manageBookMenu = new JMenu("\u56FE\u4E66\u7BA1\u7406");
		manageBookMenu.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u56FE\u4E66\u7BA1\u7406.png")));
		menuBar.add(manageBookMenu);
		
		JMenuItem manageInformationMItem = new JMenuItem("\u7BA1\u7406\u56FE\u4E66\u4FE1\u606F");
		manageInformationMItem.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u56FE\u4E66\u4FE1\u606F\u7BA1\u7406.png")));
		manageInformationMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BookInformationJdialog(bookDao.searchAllBook().get(1),bookDao.searchAllBook().get(2)).setVisible(true);
			}
		});
		manageBookMenu.add(manageInformationMItem);
		
		JMenuItem returnManageMItem = new JMenuItem("\u5F52\u8FD8\u56FE\u4E66\r\n");
		returnManageMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ReturnBookManageJdialog(lendingOfUserDao.searchAll().get(1),lendingOfUserDao.searchAll().get(2)).setVisible(true);
			}
		});
		returnManageMItem.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u501F\u51FA\u6216\u5F52\u8FD8.png")));
		manageBookMenu.add(returnManageMItem);
		
		JMenuItem renewMenuItem = new JMenuItem("\u56FE\u4E66\u7EED\u501F\r\n");
		renewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new RenewBookManageJdialog().setVisible(true);
			}
		});
		renewMenuItem.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u6CE8\u518C\u6807\u9898.png")));
		manageBookMenu.add(renewMenuItem);
		
		JMenu mnNewMenu = new JMenu("\u5176\u4ED6");
		mnNewMenu.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u5176\u4ED6.png")));
		menuBar.add(mnNewMenu);
		
		JMenuItem logOutMenuItem = new JMenuItem("\u9000\u51FA");
		//为退出事件添加监视器
		logOutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outLog.outLogActionPerform(BookManageFrame.this);
			}
		});
		logOutMenuItem.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u9000\u51FA.png")));
		mnNewMenu.add(logOutMenuItem);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setForeground(new Color(250, 235, 215));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(BookManageFrame.class.getResource("/picture/\u56FE\u4E66\u9986.png")));
		lblNewLabel.setBounds(210, 10, 217, 153);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u6B22\u8FCE\u60A8\uFF0C\u56FE\u4E66\u7BA1\u7406\u5458!");
		lblNewLabel_1.setFont(new Font("华文行楷", Font.PLAIN, 25));
		lblNewLabel_1.setBounds(199, 159, 259, 52);
		contentPane.add(lblNewLabel_1);
	}
}
