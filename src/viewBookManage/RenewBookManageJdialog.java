package viewBookManage;



import javax.swing.JButton;
import javax.swing.JDialog;

import javax.swing.table.DefaultTableModel;

import dao.RenewBookDao;
import model.RenewBook;
import util.PackagingTabletextInTextField;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.util.Vector;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RenewBookManageJdialog extends JDialog {
	private JTextField idTxt;
	private JTextField idOfBookTxt;
	private JTable table;
	private RenewBookDao dao = new RenewBookDao();
	private JTextField numberOfRenewTxt;

	/**
	 * Create the dialog.
	 */
	public RenewBookManageJdialog() {
		setBounds(100, 100, 457, 386);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 443, 168);
		getContentPane().add(scrollPane);
		
		idTxt = new JTextField();
		idTxt.setEditable(false);
		idTxt.setBounds(88, 223, 138, 21);
		getContentPane().add(idTxt);
		idTxt.setColumns(10);
		
		idOfBookTxt = new JTextField();
		idOfBookTxt.setEditable(false);
		idOfBookTxt.setColumns(10);
		idOfBookTxt.setBounds(88, 277, 138, 21);
		getContentPane().add(idOfBookTxt);
		
		JLabel lblNewLabel = new JLabel("\u7528\u6237ID\uFF1A");
		lblNewLabel.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel.setBounds(18, 223, 78, 22);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u56FE\u4E66\u7F16\u53F7\uFF1A");
		lblNewLabel_1.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(8, 279, 86, 15);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_5 = new JLabel("\u5355\u51FB\u8868\u683C\u4E2D\u7684\u67D0\u4E00\u884C\u4EE5\u83B7\u53D6\u5176\u6570\u636E");
		lblNewLabel_5.setIcon(new ImageIcon(RenewBookManageJdialog.class.getResource("/picture/\u8B66\u544A.png")));
		lblNewLabel_5.setForeground(Color.DARK_GRAY);
		lblNewLabel_5.setFont(new Font("华文行楷", Font.PLAIN, 18));
		lblNewLabel_5.setBounds(10, 180, 333, 26);
		getContentPane().add(lblNewLabel_5);
		
		JButton agreeButton = new JButton("\u540C\u610F");
		agreeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agreeAction();
			}
		});
		agreeButton.setBackground(SystemColor.controlLtHighlight);
		agreeButton.setIcon(new ImageIcon(RenewBookManageJdialog.class.getResource("/picture/\u540C\u610F.png")));
		agreeButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		agreeButton.setBounds(310, 252, 93, 22);
		getContentPane().add(agreeButton);
		
		JButton refuseButton = new JButton("\u62D2\u7EDD");
		refuseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refuseAction();
			}
		});
		refuseButton.setBackground(SystemColor.controlLtHighlight);
		refuseButton.setFont(new Font("华文楷体", Font.PLAIN, 16));
		refuseButton.setIcon(new ImageIcon(RenewBookManageJdialog.class.getResource("/picture/\u9519\u8BEF.png")));
		refuseButton.setBounds(310, 292, 93, 26);
		getContentPane().add(refuseButton);
		
		table = new JTable();
		table = new JTable();
		table.setEnabled(true);
		table.setRowSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_2 = new JLabel("\u7EED\u501F\u6B21\u6570\uFF1A");
		lblNewLabel_2.setFont(new Font("华文楷体", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(4, 324, 84, 15);
		getContentPane().add(lblNewLabel_2);
		
		numberOfRenewTxt = new JTextField();
		numberOfRenewTxt.setEditable(false);
		numberOfRenewTxt.setColumns(10);
		numberOfRenewTxt.setBounds(89, 321, 138, 21);
		getContentPane().add(numberOfRenewTxt);
		fillTable();
		listenTable();
	}
	/**
	 * 拒绝事件的处理
	 */
	private void refuseAction() {
		if(!idOfBookTxt.getText().equals("")) {
			RenewBook renewBook = new RenewBook();
			renewBook.setId(idTxt.getText());
			renewBook.setIdOfBook(idOfBookTxt.getText());
			boolean result = dao.refuseRenew(renewBook);
			refresh();
			if(result) {
				JOptionPane.showMessageDialog(null, "操作成功！");
			}else {
				JOptionPane.showMessageDialog(null, "操作失败", "警告", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(null, "请选中表中要操作的数据的所在行", "警告", JOptionPane.WARNING_MESSAGE);
		}
	}
	/**
	 * 同意续借事件的处理
	 */
	private void agreeAction() {
		if(!idOfBookTxt.getText().equals("")) {
			RenewBook renewBook = new RenewBook();
			renewBook.setId(idTxt.getText());
			renewBook.setIdOfBook(idOfBookTxt.getText());
			renewBook.setNumberOfRenew(Integer.parseInt(numberOfRenewTxt.getText()));
			boolean result = dao.agreeRenew(renewBook);
			refresh();
			if(result) {
				JOptionPane.showMessageDialog(null, "续借成功！");
				
			}else {
				JOptionPane.showMessageDialog(null, "操作失败", "警告", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(null, "请选中表中要操作的数据的所在行", "警告", JOptionPane.WARNING_MESSAGE);
		}
	}
	/**
	 * 为表格组件添加监听器
	 */
	private void listenTable() {
		table.addMouseListener(new PackagingTabletextInTextField(this.table, packTextFields()));
	}
	/**
	 * 填充表格
	 */
	private void fillTable() {
		DefaultTableModel model = (DefaultTableModel)table.getModel();
		model.setDataVector(dao.searchAll().get("data"), dao.searchAll().get("column"));
		table.setModel(model);
	}
	/**
	 * 把文本框装进向量中
	 * @return
	 */
	private Vector<JTextField> packTextFields(){
		Vector<JTextField> vector = new Vector<JTextField>();
		vector.add(idOfBookTxt);
		vector.add(idTxt);
		vector.add(numberOfRenewTxt);
		return vector;
	}
	/**
	 * 刷新表格和文本框
	 */
	private void refresh() {
		fillTable();
		idTxt.setText("");
		idOfBookTxt.setText("");
		numberOfRenewTxt.setText("");
	}
}
